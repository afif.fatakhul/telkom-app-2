<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function () {
    // Dashboard
    Route::get('/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');

    // Profile
    Route::get('/profile', [App\Http\Controllers\Admin\ProfileController::class, 'index']);
    Route::post('/profile/tambah', [App\Http\Controllers\Admin\ProfileController::class, 'store'])->name('profileTambah');
    Route::get('/profile/{id}/edit', [App\Http\Controllers\Admin\ProfileController::class, 'edit']);
    Route::post('/profile/hapus', [App\Http\Controllers\Admin\ProfileController::class, 'destroy'])->name('profileHapus');

    // Data Potency
    Route::post('/data-potency/cek/', [App\Http\Controllers\Admin\DataPotencyController::class, 'cek'])->name('dpCek');
    Route::get('/data-potency', [App\Http\Controllers\Admin\DataPotencyController::class, 'index']);
    Route::post('/data-potency/tambah', [App\Http\Controllers\Admin\DataPotencyController::class, 'store'])->name('dpTambah');
    Route::post('/data-potency/import', [App\Http\Controllers\Admin\DataPotencyController::class, 'import'])->name('dpImport');
    Route::get('/data-potency/{id}/edit', [App\Http\Controllers\Admin\DataPotencyController::class, 'edit']);
    Route::post('/data-potency/hapus', [App\Http\Controllers\Admin\DataPotencyController::class, 'destroy'])->name('dpHapus');
    Route::post('/graphPKS', [App\Http\Controllers\Admin\DataPotencyController::class, 'graphPKS'])->name('graphPKS');
    Route::post('/graphCategory', [App\Http\Controllers\Admin\DataPotencyController::class, 'graphCategory'])->name('graphCategory');
    Route::post('/graphOwner', [App\Http\Controllers\Admin\DataPotencyController::class, 'graphOwner'])->name('graphOwner');

    // Data ABD
    Route::post('/data-abd/get/', [App\Http\Controllers\Admin\ABDController::class, 'getSiteName'])->name('getSiteName');
    Route::get('/data-abd', [App\Http\Controllers\Admin\ABDController::class, 'index']);
    Route::post('/data-abd/tambah', [App\Http\Controllers\Admin\ABDController::class, 'store'])->name('abdTambah');
    Route::get('/data-abd/detail/{id}', [App\Http\Controllers\Admin\ABDController::class, 'detail']);
    Route::get('/data-abd/{id}/edit', [App\Http\Controllers\Admin\ABDController::class, 'edit']);
    Route::post('/data-abd/hapus', [App\Http\Controllers\Admin\ABDController::class, 'destroy'])->name('abdHapus');
    // Hardware
    Route::post('/hardware/cek/', [App\Http\Controllers\Admin\HardwareController::class, 'cek'])->name('hardwareCek');
    Route::get('/hardware', [App\Http\Controllers\Admin\HardwareController::class, 'index']);
    Route::post('/hardware/tambah', [App\Http\Controllers\Admin\HardwareController::class, 'store'])->name('hardwareTambah');
    Route::post('/hardware/import', [App\Http\Controllers\Admin\HardwareController::class, 'import'])->name('hardwareImport');
    Route::get('/hardware/{id}/edit', [App\Http\Controllers\Admin\HardwareController::class, 'edit']);
    Route::post('/hardware/hapus', [App\Http\Controllers\Admin\HardwareController::class, 'destroy'])->name('hardwareHapus');
    Route::post('/hardwareReq/tambah', [App\Http\Controllers\Admin\HardwareController::class, 'storeReq'])->name('hardwareTambahReq');
    Route::post('/hardwareReq/import', [App\Http\Controllers\Admin\HardwareController::class, 'importReq'])->name('hardwareImportReq');
    Route::get('/hardwareReq/{id}/edit', [App\Http\Controllers\Admin\HardwareController::class, 'editReq']);
    Route::post('/hardwareReq/hapus', [App\Http\Controllers\Admin\HardwareController::class, 'destroyReq'])->name('hardwareHapusReq');

    // Dashboard
    Route::get('/payload_potency', [App\Http\Controllers\Admin\DashboardController::class, 'topWorstPayload']);
    Route::get('/ebit_single', [App\Http\Controllers\Admin\DashboardController::class, 'ebitMarginSinggle']);
    Route::get('/building_type', [App\Http\Controllers\Admin\DashboardController::class, 'buildingType']);

    Route::get('/about', function () {
        return view('about');
    })->name('about');
});

Route::middleware(['auth', 'cekuser:admin'])->group(function () {
    // Data User
    Route::post('/data-users/cek/', [App\Http\Controllers\Admin\UserController::class, 'cek'])->name('userCek');
    Route::get('/data-users', [App\Http\Controllers\Admin\UserController::class, 'index']);
    Route::post('/data-users/tambah', [App\Http\Controllers\Admin\UserController::class, 'store'])->name('userTambah');
    Route::get('/data-users/{id}/edit', [App\Http\Controllers\Admin\UserController::class, 'edit']);
    Route::post('/data-users/hapus', [App\Http\Controllers\Admin\UserController::class, 'destroy'])->name('userHapus');
});

Route::middleware(['auth', 'cekuser:admin'])->group(function () {

    // Program
    Route::post('/program/cek/', [App\Http\Controllers\Admin\ProgramController::class, 'cek'])->name('programCek');
    Route::get('/program', [App\Http\Controllers\Admin\ProgramController::class, 'index']);
    Route::post('/program/tambah', [App\Http\Controllers\Admin\ProgramController::class, 'store'])->name('programTambah');
    Route::get('/program/{id}/edit', [App\Http\Controllers\Admin\ProgramController::class, 'edit']);
    Route::post('/program/hapus', [App\Http\Controllers\Admin\ProgramController::class, 'destroy'])->name('programHapus');
    // Proposal
    Route::get('/proposal', [App\Http\Controllers\Admin\ProposalController::class, 'index']);
    Route::post('/proposal/tambah', [App\Http\Controllers\Admin\ProposalController::class, 'store'])->name('pTambah');
    Route::post('/proposal/import', [App\Http\Controllers\Admin\ProposalController::class, 'import'])->name('pImport');
    Route::get('/proposal/{id}/edit', [App\Http\Controllers\Admin\ProposalController::class, 'edit']);
    Route::post('/proposal/hapus', [App\Http\Controllers\Admin\ProposalController::class, 'destroy'])->name('pHapus');

    // Feasibility
    Route::get('/feasibility-calculator', [App\Http\Controllers\Admin\FeasibilityController::class, 'calculator']);
    Route::get('/feasibility_exportexcel', [App\Http\Controllers\Admin\FeasibilityController::class, 'exportExcel']);
    Route::get('/import_feasibility-calculator', [App\Http\Controllers\Admin\FeasibilityController::class, 'importView']);
    Route::post('/proses-import_feasibility', [App\Http\Controllers\Admin\FeasibilityController::class, 'prosesImport']);
    Route::get('/export_feasibility', [App\Http\Controllers\Admin\FeasibilityController::class, 'exportCsv']);

    // Snipper
    Route::resource("/sniper_activity", App\Http\Controllers\Admin\SniperActivityController::class);
    Route::get('/fetch_sniper_activity', [App\Http\Controllers\Admin\SniperActivityController::class, 'fetchData']);
    Route::get('/fetch_sites', [App\Http\Controllers\Admin\SniperActivityController::class, 'fetchSites']);
    Route::get('/put_site_name',  [App\Http\Controllers\Admin\SniperActivityController::class, 'putSiteName']);
    Route::post('/import_sniper',  [App\Http\Controllers\Admin\SniperActivityController::class, 'import']);

    // Request
    Route::resource("/request_sniper", App\Http\Controllers\Admin\RequestSniperController::class);
    Route::get("/approve_request_sniper/{id}", [App\Http\Controllers\Admin\RequestSniperController::class, 'approve']);
    Route::get('/fetch_request_sniper', [App\Http\Controllers\Admin\RequestSniperController::class, 'fetchData']);

    // Equipment
    Route::resource("/equipment", App\Http\Controllers\Admin\EquipmentController::class);
    Route::get('/fetch_equipment', [App\Http\Controllers\Admin\EquipmentController::class, 'fetchData']);
    Route::post('/import_equipment',  [App\Http\Controllers\Admin\EquipmentController::class, 'import']);
});
