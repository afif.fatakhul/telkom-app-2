@extends('layouts.master')

@section('content')
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{url('data-abd')}}">Data ABD</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Detail</li>
        </ol>
    </nav>
    <div class="col-12">
        <div class="card">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-12">
                            <h6>Detail ABD
                                <a href="{{url('data-abd')}}" class="btn btn-secondary" style="float: right;"><i class="fas fa-arrow-left me-2"></i> Kembali</a>
                            </h6>
                            Site : {{$abd->site_id}} - {{$abd->site_name}} <br>
                            File : {{$abd->file}} <br>
                            Ket : <a href="{{$abd->ket}}" target="_blank">{{$abd->ket}}</a> <br>
                            Waktu Unggah : {{$abd->created_at}}
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0">
                    @if($abd->file!="")
                    <iframe src="{{asset('file-abd/'.$abd->file)}}" width="100%" height="500"></iframe>
                    @endif
                </div>
            </div>

            </section>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush