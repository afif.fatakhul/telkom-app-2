@extends('layouts.master')
@section('content')
<style>
    .close {
        border: 0px;
    }
</style>
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Request Sniper</li>
        </ol>
    </nav>
    <div class="card card-body">
    <div class="table-responsive pb-2">
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-primary" id="btnTambah">Tambah <span class="fa fa-plus"></span></button>
            </div>
        </div>
        <table id="tabelSniperActivity" class="table align-items-center mb-0">
            <thead class="table table-dark">
                <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">No.</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site ID</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site Name</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Multiname</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Region</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<!-- Modal Tambah / Edit -->
<div class="modal fade" id="modalTambahEdit" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      </div>
      <div class="modal-body">
            <form id="sniperSubmit">  
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Site ID</label>
                            <div id="siteIdContainer">
                                <select class="siteId form-control" id="site_id" name="site_id"></select>
                            </div>
                            <div class="input-group" id="siteIdEdit">
                                <input type="text" name="site_id" class="form-control" id="site_id2" />
                                <span class="input-group-text cursor-pointer" id="changeSiteId">Ganti</span>
                            </div>
                            <div id="err_site_id" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Site Name</label>
                            <input class="form-control" name="site_name" id="site_name" readonly />
                            <div id="err_site_name" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Multiname</label>
                    <input class="form-control" id="multiname" name="multiname" />
                    <div id="err_multiname" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Region</label>
                    <input type="text" class="form-control" id="region" name="region" />
                    <div id="err_region" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>SOW</label>
                    <input type="text" class="form-control" id="sow" name="sow" />
                    <div id="err_sow" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <input class="form-control" type="date" id="start_date" name="start_date" />
                    <div id="err_start_date" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Done Date</label>
                    <input class="form-control" type="date" id="done_date" name="done_date" />
                    <div id="err_done_date" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Link Report</label>
                    <input type="text" class="form-control" id="link_report" name="link_report" />
                    <div id="err_link_report" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <button type="submit" id="btnSimpan" class="btn btn-primary">Simpan</button>
                    <button type="submit" id="btnLoading" class="btn btn-info hide">Loading....</button>
                </div>
            </div>
        </form>
    </div>
  </div>
</div>

<!-- modal detail -->
<div class="modal fade bd-example-modal-lg" id="detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailLabel">Detail Data Request Sniper</h5>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="table-detail">
                    <tbody>
                        <tr>
                            <th scope="col">Site ID</th>
                            <td width="4">:</td>
                            <td id="siteId"></td>
                        </tr>
                        <tr>
                            <th scope="col">Site Name</th>
                            <td width="4">:</td>
                            <td id="siteName2"></td>
                        </tr>
                        <tr>
                            <th scope="col">Multiname</th>
                            <td width="4">:</td>
                            <td id="multiName"></td>
                        </tr>
                        <tr>
                            <th scope="col">Region</th>
                            <td width="4">:</td>
                            <td id="reg"></td>
                        </tr>
                        <tr>
                            <th scope="col">Sow</th>
                            <td width="4">:</td>
                            <td id="sw"></td>
                        </tr>
                        <tr>
                            <th scope="col">Start Date</th>
                            <td width="4">:</td>
                            <td id="startDate"></td>
                        </tr>
                        <tr>
                            <th scope="col">Done Date</th>
                            <td width="4">:</td>
                            <td id="doneDate"></td>
                        </tr>
                        <tr>
                            <th scope="col">Year Done</th>
                            <td width="4">:</td>
                            <td id="yearDone"></td>
                        </tr>
                        <tr>
                            <th scope="col">Link Report</th>
                            <td width="4">:</td>
                            <td id="linkReport"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
        var sniperId = "";
        var urlAction = "";
        var methodAction = "";
        var role = '{{ Auth::user()->role }}';
        var tabelSniperActivity = $("#tabelSniperActivity").DataTable({
            ajax: '{{ url("/fetch_request_sniper") }}',
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv'
            ],
            order: [[0, "asc"]],
            columns: [
                {
                    data: "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: "site_id"
                },
                {
                    data: "site_name"
                },
                {
                    data: "multiname"
                },
                {
                    data: "region"
                },
                {
                    data: "id",
                    render: function(data, type, row, meta) {
                          return type === "display"
                            ? role === "admin" ?' <button id="btnApprove" class="btn btn-sm btn-success">Approve</button> '+ 
                            '<button id="btnDetail" class="btn btn-sm btn-primary"><span class="fa fa-eye"></span></button> '+
                            '<button id="btnEdit" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></button> ' +
                            '<button id="btnDelete" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span></button>' :
                            '<button id="btnDetail" class="btn btn-sm btn-primary"><span class="fa fa-eye"></span></button> '+
                            '<button id="btnEdit" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></button> ' +
                            '<button id="btnDelete" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span></button>'
                            : data;
                    }
                }
            ]
        });


        $('.siteId').select2({
            width: '100%',
            placeholder: 'Select an item',
            ajax: {
                url: '{{ url("/fetch_sites") }}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.site_id,
                            id: item.site_id
                        }
                        })
                    };
                },
                cache: true
            }
        }).on('select2:select', function (evt) {
            var data = $(".siteId option:selected").text();
            putSiteName(data);
        });

        function putSiteName(siteId) {
            $.ajax({
                url: '{{ url("/put_site_name") }}',
                type: "GET",
                data: {siteId:siteId},
                success: function(response){
                    $("#site_name").val(response);
                    $("#site_id2").val(siteId);
                }
            })
        }

        $('#tabelSniperActivity tbody').on( 'click', 'button#btnDetail', function () {
            $("#detail").modal("show");
            var data = tabelSniperActivity.row( $(this).parents('tr') ).data();
            $("#kode").html(data.kode);
            $("#siteId").html(data.site_id);
            $("#siteName2").html(data.site_name);
            $("#multiName").html(data.multiname);
            $("#reg").html(data.region);
            $("#sw").html(data.sow);
            $("#startDate").html(data.start_date);
            $("#doneDate").html(data.done_date);
            var d = new Date(data.done_date);
            $("#yearDone").html(d.getFullYear());
            $("#linkReport").html(data.link_report);
        });

        $("#btnTambah").on('click', function(){
            urlAction =  '{{ url("/request_sniper") }}';
            methodAction = "POST";
            $("#staticBackdropLabel").html("Tambah Request Sniper");
            $("#siteIdEdit").addClass("hide");
            $("#siteIdContainer").removeClass("hide");
            $('#sniperSubmit')[0].reset();
            $("#modalTambahEdit").modal("show");
        });

        $('#tabelSniperActivity tbody').on('click', 'button#btnApprove', function(){
            var data = tabelSniperActivity.row( $(this).parents('tr') ).data();
            urlAction =  '{{ url("/approve_request_sniper") }}'+'/'+data.id;
            methodAction = "GET";
            Swal.fire({
                title: 'Yakin menyetujui request ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
                }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: urlAction,
                        type: methodAction,
                        success: function(res){
                            sniperId = "";
                            tabelSniperActivity.ajax.reload();
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil disetujui.',
                                'success'
                            );
                        }
                    });
                }
            });
        });

        $("#formImport").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                type: methodAction,
                url: urlAction,
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                   $(".btnSubmit").addClass("hide");
                   $(".btnLoading").removeClass("hide");
                },
                success: function(response){
                    $(".btnSubmit").removeClass("hide");
                    $(".btnLoading").addClass("hide");
                    sniperId = "";
                    tabelSniperActivity.ajax.reload();
                    Swal.fire(
                        'Berhasil!',
                        'Data berhasil diimport.',
                        'success'
                    );
                    $("#import").modal("hide");
                },
                error : function(err){
                    $(".btnSubmit").removeClass("hide");
                    $(".btnLoading").addClass("hide");
                    if(err.status == 422){
                        $.each(err.responseJSON.errors, function(i,error){
                            $('#err_'+i).html(error[0]);
                            $('#file').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $('#tabelSniperActivity tbody').on('click', 'button#btnDelete', function(){
            var data = tabelSniperActivity.row( $(this).parents('tr') ).data();
            sniperId = data.id;
            urlAction =  '{{ url("/request_sniper") }}'+'/'+sniperId;
            methodAction = "DELETE";
            Swal.fire({
                title: 'Yakin menghapus data ini?',
                text: "Data yang dihapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
                }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: urlAction,
                        type: methodAction,
                        success: function(res){
                            sniperId = "";
                            tabelSniperActivity.ajax.reload();
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil dihapus.',
                                'success'
                            );
                        }
                    });
                }
            });
        });

        $("#changeSiteId").on('click', function(){
            $("#siteIdEdit").addClass("hide");
            $("#siteIdContainer").removeClass("hide");
        });

        $('#tabelSniperActivity tbody').on( 'click', 'button#btnEdit', function () {
            $("#modalTambahEdit").modal("show");
            $("#staticBackdropLabel").html("Edit Request Sniper");
            var data = tabelSniperActivity.row( $(this).parents('tr') ).data();
            sniperId = data.id;
            urlAction =  '{{ url("/request_sniper") }}'+'/'+sniperId;
            methodAction = "PUT";
            $("#siteIdContainer").addClass("hide");
            $("#siteIdEdit").removeClass("hide");
            $("#site_id2").val(data.site_id);
            $("#site_name").val(data.site_name);
            $("#multiname").val(data.multiname);
            $("#region").val(data.region);
            $("#sow").val(data.sow);
            $("#start_date").val(data.start_date);
            $("#done_date").val(data.done_date);
            $("#link_report").val(data.link_report);
        });

       $("#sniperSubmit").submit(function(e){
            e.preventDefault();
            $("#btnSimpan").addClass("hide");
            $("#btnLoading").removeClass("hide");
            var formData = $(this);
            $.ajax({
                url: urlAction,
                type: methodAction,
                data: formData.serialize(),
                success: function(res){
                    $("#btnSimpan").removeClass("hide");
                    $("#btnLoading").addClass("hide");
                    $("#modalTambahEdit").modal("hide");
                    $('#sniperSubmit')[0].reset();
                    sniperId = "";
                    tabelSniperActivity.ajax.reload();
                    swal("Berhasil", "Data berhasil disimpan", "success");
                },
                error : function(err){
                    $("#btnSimpan").removeClass("hide");
                    $("#btnLoading").addClass("hide");
                    if(err.status == 422){
                        $.each(err.responseJSON.errors, function(i,error){
                            $('#err_'+i).html(error[0]);
                            $('#site_id').addClass('is-invalid');
                            $('#site_name').addClass('is-invalid');
                            $('#multiname').addClass('is-invalid');
                            $('#region').addClass('is-invalid');
                            $('#sow').addClass('is-invalid');
                            $('#done_date').addClass('is-invalid');
                            $('#start_date').addClass('is-invalid');
                            $('#link_report').addClass('is-invalid');
                        });
                    }
                }
            });
        });

</script>
@endpush