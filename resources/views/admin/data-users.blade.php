@extends('layouts.master')
@section('content')
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Data Users</li>
        </ol>
    </nav>
    <div class="col-12">
        <div class="card mb-4">
            <div class="row">
                <div class="col-6">
                    <div class="card-header pb-0">
                        <h6>Data Users</h6>
                        <a href="javascript:void(0)" id="btnTambah" class="btn btn-primary">
                            <i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah</a>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-1 pb-2">
                <div class="container">
                    <div class="table-responsive pb-2">
                        <table id="tabel" class="table align-items-center mb-0">
                            <thead class="table table-dark">
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">No.</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Email</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Role</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($user as $data)
                                <tr>
                                    <td><span class="text-xs font-weight-bold">{{$no++}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->name }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->email }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->role }}</span></td>
                                    <td class="align-middle">
                                        <a href="javascript:void(0)" class="btn btn-warning mb-0" id="btnEdit" data-toggle="modal" data-id="{{ $data->id }}"><i class="fas fa-pencil-alt me-2" aria-hidden="true"></i>Edit</a>
                                        <meta name=" csrf-token" content="{{ csrf_token() }}">
                                        <a href="javascript:void(0)" class="btn btn-danger mb-0" id="btnHapus" data-toggle="modal" data-id="{{ $data->id }}"><i class="far fa-trash-alt me-2"></i> Hapus</a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade bd-example-modal-lg" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambahLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahLabel"></h5>
            </div>
            <form action="{{route('userTambah')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="action" id="action" value="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Name</label>
                                <input type="text" id="name" name="name" placeholder="Masukkan Name" class="form-control" onchange="validateNama()" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Email</label>
                                <input type="email" id="email" name="email" placeholder="Masukkan Email" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Password</label>
                                <input type="password" id="password" name="password" placeholder="Masukkan Password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Role</label>
                                <select name="role" id="role" class="form-control">
                                    <option value="admin">Super Admin</option>
                                    <option value="tp">TP</option>
                                    <option value="guest">Guest</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal tambah & edit -->
<!-- modal hapus -->
<div class="modal fade bd-example-modal-lg" id="hapus" tabindex="-1" role="dialog" aria-labelledby="hapusLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hapusLabel"></h5>
            </div>
            <form action="{{route('userHapus')}}" method="POST">
                @csrf
                <input type="hidden" name="id1" id="id1">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ya</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    function validateNama() {
        var name = document.getElementById("name").value;
        $.post('{{ route("userCek") }}', {
            _token: '{{csrf_token()}}',
            name: name
        }, function(data) {
            if (data.length == 0) {
                return true;
            } else {
                swal("Error", "Name Sudah Ada!", "error");
                $('#name').val('');
            }
        });
    }

    $(document).ready(function() {
        $('#tabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv'
            ]
        });

        // modal tambah
        $(document).on('click', '#btnTambah', function(e) {
            $('#tambahLabel').html("Tambah Data Users");
            $('#tambah').modal('show');
            $('input[name=action]').val('tambah');
            $('#id').val("");
            $('#name').val("");
            $('#email').val("");
            $('#password').val("");
        });

        // modal edit
        $('body').on('click', '#btnEdit', function() {
            var data_id = $(this).data('id');
            $.get('data-users/' + data_id + '/edit', function(data) {
                $('#tambahLabel').html("Edit Data Users");
                $('#btn-save').prop('disabled', false);
                $('#tambah').modal('show');
                $('input[name=action]').val('edit');
                $('#id').val(data.id);
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#role').val(data.role);
            })
        });

        // modal hapus
        $('body').on('click', '#btnHapus', function() {
            var data_id = $(this).data('id');
            $.get('data-users/' + data_id + '/edit', function(data) {
                $('#hapusLabel').html("Hapus Data User");
                $('#btn-save').prop('disabled', false);
                $('#hapus').modal('show');
                $('#id1').val(data.id);
            })
        });
    });
</script>
@endpush