@extends('layouts.master')
@section('content')
<style>
    .close {
        border: 0px;
    }
</style>
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Equipment</li>
        </ol>
    </nav>
    <div class="card card-body">
    <div class="table-responsive pb-2">
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-primary" id="btnTambah">Tambah <span class="fa fa-plus"></span></button>
                @if(Auth::user()->role == 'admin')
                    <button class="btn btn-secondary" id="btnImport">Import <i class="fas fa-file"></i></button>
                @endif
            </div>
            <div class="col-md-6">
             @if(Auth::user()->role == 'admin')
                <a style="float: right;" href="{{ asset('import_datas/Import_Sniper_Format.xlsx') }}" class="btn btn-success">
                <i class="fas fa-download"></i> Download Draft Import</a>
             @endif
            </div>
        </div>
        <table id="tabelEquipment" class="table align-items-center mb-0">
            <thead class="table table-dark">
                <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">No.</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Onair Date</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site ID</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Combat Sniper</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Node B Name</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Node B Status</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Latitude</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Longitude</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Address</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">TX Type</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Remark Issue</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Remark Site Existing</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<!-- Modal Tambah / Edit -->
<div class="modal fade" id="modalTambahEdit" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">&times;</button>
      </div>
      <div class="modal-body">
            <form id="equipmentSubmit">  
                <div class="form-group">
                    <label>Onair Date</label>
                    <input class="form-control" type="date" id="onair_date" name="onair_date" />
                    <div id="err_onair_date" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Site ID</label>
                    <div id="siteIdContainer">
                        <select class="siteId form-control" id="site_id" name="site_id"></select>
                    </div>
                    <div class="input-group" id="siteIdEdit">
                        <input type="text" name="site_id" class="form-control" id="site_id2" />
                        <span class="input-group-text cursor-pointer" id="changeSiteId">Ganti</span>
                    </div>
                    <div id="err_site_id" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Combat Sniper</label>
                    <input class="form-control" id="combat_sniper" name="combat_sniper" />
                    <div id="err_combat_sniper" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Node B Name</label>
                    <input type="text" class="form-control" id="node_b_name" name="node_b_name" />
                    <div id="err_node_b_name" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Node B Status</label>
                    <input type="text" class="form-control" id="node_b_status" name="node_b_status" />
                    <div id="err_node_b_status" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Latitude</label>
                    <input class="form-control" type="text" id="latitude" name="latitude" />
                    <div id="err_latitude" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Longitude</label>
                    <input type="text" class="form-control" id="longitude" name="longitude" />
                    <div id="err_longitude" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control" id="address" name="address" />
                    <div id="err_address" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>TX Type</label>
                    <input type="text" class="form-control" id="tx_type" name="tx_type" />
                    <div id="err_tx_type" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Remark Issue</label>
                    <input type="text" class="form-control" id="remark_issue" name="remark_issue" />
                    <div id="err_remark_issue" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label>Remark Site Existing</label>
                    <input type="text" class="form-control" id="remark_site_existing" name="remark_site_existing" />
                    <div id="err_remark_site_existing" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <button type="submit" id="btnSimpan" class="btn btn-primary">Simpan</button>
                    <button type="submit" id="btnLoading" class="btn btn-info hide">Loading....</button>
                </div>
            </div>
        </form>
    </div>
  </div>
</div>

<!-- modal detail -->
<div class="modal fade bd-example-modal-lg" id="detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailLabel">Detail Data Equipment</h5>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="table-detail">
                    <tbody>
                        <tr>
                            <th scope="col">Onair Date</th>
                            <td width="4">:</td>
                            <td id="onAirDate"></td>
                        </tr>
                        <tr>
                            <th scope="col">Site ID</th>
                            <td width="4">:</td>
                            <td id="siteId"></td>
                        </tr>
                        <tr>
                            <th scope="col">Combat Sniper</th>
                            <td width="4">:</td>
                            <td id="combatSniper"></td>
                        </tr>
                        <tr>
                            <th scope="col">Node B Name</th>
                            <td width="4">:</td>
                            <td id="nodeBName"></td>
                        </tr>
                        <tr>
                            <th scope="col">Node B Status</th>
                            <td width="4">:</td>
                            <td id="nodeBStatus"></td>
                        </tr>
                        <tr>
                            <th scope="col">Latitude</th>
                            <td width="4">:</td>
                            <td id="latitudeDet"></td>
                        </tr>
                        <tr>
                            <th scope="col">Longitude</th>
                            <td width="4">:</td>
                            <td id="longitudeDet"></td>
                        </tr>
                        <tr>
                            <th scope="col">Address</th>
                            <td width="4">:</td>
                            <td id="addressDet"></td>
                        </tr>
                        <tr>
                            <th scope="col">Tx Type</th>
                            <td width="4">:</td>
                            <td id="txType"></td>
                        </tr>
                        <tr>
                            <th scope="col">Remark Issue</th>
                            <td width="4">:</td>
                            <td id="remarkIssue"></td>
                        </tr>
                        <tr>
                            <th scope="col">Remark Site Existing</th>
                            <td width="4">:</td>
                            <td id="remarkSiteExisting"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Import -->
<div class="modal fade bd-example-modal-lg" id="import">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importLabel">Import Data</h5>
            </div>
            <form id="formImport" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Pilih File (*excel)</label>
                                <input type="file" id="file" name="file" placeholder="Masukkan File" class="form-control" accept="application/vnd.ms-excel,.xlsx">
                                <div id="err_file" class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
                        <button type="submit" class="btn btn-primary btnLoading hide">Loading....</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
        var sniperId = "";
        var urlAction = "";
        var methodAction = "";
        var role = '{{ Auth::user()->role }}';

        var tabelEquipment = $("#tabelEquipment").DataTable({
            ajax: '{{ url("/fetch_equipment") }}',
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv'
            ],
            order: [[0, "asc"]],
            columns: [
                {
                    data: "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: "onair_date"
                },
                {
                    data: "site_id"
                },
                {
                    data: "combat_sniper"
                },
                {
                    data: "node_b_name"
                },
                {
                    data: "node_b_status"
                },
                {
                    data: "latitude"
                },
                {
                    data: "longitude"
                },
                {
                    data: "address"
                },
                {
                    data: "tx_type"
                },
                {
                    data: "remark_issue"
                },
                {
                    data: "remark_site_existing"
                },
                {
                    data: "id",
                    render: function(data, type, row, meta) {
                        // return type === "display"
                        //     ?  role === "admin" ? 
                        //     '<button id="btnDetail" class="btn btn-sm btn-primary"><span class="fa fa-eye"></span></button> '+
                        //     '<button id="btnEdit" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></button> ' +
                        //     '<button id="btnDelete" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span></button>' : ''
                        //     : data;
                         return type === "display"
                            ?  
                            '<button id="btnDetail" class="btn btn-sm btn-primary"><span class="fa fa-eye"></span></button> '+
                            '<button id="btnEdit" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></button> ' +
                            '<button id="btnDelete" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span></button>' 
                            : data;
                    }
                }
            ]
        });


        $('.siteId').select2({
            width: '100%',
            placeholder: 'Select an item',
            ajax: {
                url: '{{ url("/fetch_sites") }}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.site_id,
                            id: item.site_id
                        }
                        })
                    };
                },
                cache: true
            }
        }).on('select2:select', function (evt) {
            var data = $(".siteId option:selected").text();
            putSiteName(data);
        });

        function putSiteName(siteId) {
            $.ajax({
                url: '{{ url("/put_site_name") }}',
                type: "GET",
                data: {siteId:siteId},
                success: function(response){
                    $("#site_name").val(response);
                    $("#site_id2").val(siteId);
                }
            })
        }

        $('#tabelEquipment tbody').on( 'click', 'button#btnDetail', function () {
            $("#detail").modal("show");
            var data = tabelEquipment.row( $(this).parents('tr') ).data();
            $("#siteId").html(data.site_id);
            $("#onairDate").html(data.onair_date);
            $("#combatSniper").html(data.combat_sniper);
            $("#nodeBName").html(data.node_b_name);
            $("#nodeBStatus").html(data.node_b_status);
            $("#latitudeDet").html(data.latitude);
            $("#longitudeDet").html(data.longitude);
            $("#addressDet").html(data.address);
            $("#txType").html(data.tx_type);
            $("#remarkIssue").html(data.remark_issue);
            $("#remarkSiteExisting").html(data.remark_site_existing);
        });

        $("#btnTambah").on('click', function(){
            urlAction =  '{{ url("/equipment") }}';
            $("#staticBackdropLabel").html("Tambah Data Equipment");
            methodAction = "POST";
            $("#siteIdEdit").addClass("hide");
            $("#siteIdContainer").removeClass("hide");
            $('#equipmentSubmit')[0].reset();
            $("#modalTambahEdit").modal("show");
        });

        $("#btnImport").on('click', function(){
            urlAction =  '{{ url("/import_equipment") }}';
            methodAction = "POST";
            $("#import").modal("show");
        });

        $("#formImport").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                type: methodAction,
                url: urlAction,
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                   $(".btnSubmit").addClass("hide");
                   $(".btnLoading").removeClass("hide");
                },
                success: function(response){
                    $(".btnSubmit").removeClass("hide");
                    $(".btnLoading").addClass("hide");
                    sniperId = "";
                    tabelEquipment.ajax.reload();
                    Swal.fire(
                        'Berhasil!',
                        'Data berhasil diimport.',
                        'success'
                    );
                    $("#import").modal("hide");
                },
                error : function(err){
                    $(".btnSubmit").removeClass("hide");
                    $(".btnLoading").addClass("hide");
                    if(err.status == 422){
                        $.each(err.responseJSON.errors, function(i,error){
                            $('#err_'+i).html(error[0]);
                            $('#file').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $('#tabelEquipment tbody').on('click', 'button#btnDelete', function(){
            var data = tabelEquipment.row( $(this).parents('tr') ).data();
            sniperId = data.id;
            urlAction =  '{{ url("/equipment") }}'+'/'+sniperId;
            methodAction = "DELETE";
            Swal.fire({
                title: 'Yakin menghapus data ini?',
                text: "Data yang dihapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
                }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: urlAction,
                        type: methodAction,
                        success: function(res){
                            sniperId = "";
                            tabelEquipment.ajax.reload();
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil dihapus.',
                                'success'
                            );
                        }
                    });
                }
            });
        });

        $("#changeSiteId").on('click', function(){
            $("#siteIdEdit").addClass("hide");
            $("#siteIdContainer").removeClass("hide");
        });

        $('#tabelEquipment tbody').on( 'click', 'button#btnEdit', function () {
            $("#modalTambahEdit").modal("show");
            $("#staticBackdropLabel").html("Edit Data Equipment");
            var data = tabelEquipment.row( $(this).parents('tr') ).data();
            sniperId = data.id;
            urlAction =  '{{ url("/equipment") }}'+'/'+sniperId;
            methodAction = "PUT";
            $("#siteIdContainer").addClass("hide");
            $("#siteIdEdit").removeClass("hide");
            $("#site_id2").val(data.site_id);
            $("#onair_date").val(data.onair_date);
            $("#combat_sniper").val(data.combat_sniper);
            $("#node_b_name").val(data.node_b_name);
            $("#node_b_status").val(data.node_b_status);
            $("#latitude").val(data.latitude);
            $("#longitude").val(data.longitude);
            $("#address").val(data.address);
            $("#tx_type").val(data.tx_type);
            $("#remark_issue").val(data.remark_issue);
            $("#remark_site_existing").val(data.remark_site_existing);
        });

       $("#equipmentSubmit").submit(function(e){
            e.preventDefault();
            $("#btnSimpan").addClass("hide");
            $("#btnLoading").removeClass("hide");
            var formData = $(this);
            $.ajax({
                url: urlAction,
                type: methodAction,
                data: formData.serialize(),
                success: function(res){
                    $("#btnSimpan").removeClass("hide");
                    $("#btnLoading").addClass("hide");
                    $("#modalTambahEdit").modal("hide");
                    $('#equipmentSubmit')[0].reset();
                    sniperId = "";
                    tabelEquipment.ajax.reload();
                    swal("Berhasil", "Data berhasil disimpan", "success");
                },
                error : function(err){
                    $("#btnSimpan").removeClass("hide");
                    $("#btnLoading").addClass("hide");
                    if(err.status == 422){
                        $.each(err.responseJSON.errors, function(i,error){
                            $('#err_'+i).html(error[0]);
                            $('#site_id').addClass('is-invalid');
                            $('#onair_date').addClass('is-invalid');
                            $('#combat_sniper').addClass('is-invalid');
                            $('#node_b_name').addClass('is-invalid');
                            $('#node_b_status').addClass('is-invalid');
                            $('#latitude').addClass('is-invalid');
                            $('#longitude').addClass('is-invalid');
                            $('#address').addClass('is-invalid');
                            $('#tx_type').addClass('is-invalid');
                            $('#remark_issue').addClass('is-invalid');
                            $('#remark_site_existing').addClass('is-invalid');
                        });
                    }
                }
            });
        });

</script>
@endpush