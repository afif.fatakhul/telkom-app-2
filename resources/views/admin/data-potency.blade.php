@extends('layouts.master')
@section('content')
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Data Potency</li>
        </ol>
    </nav>
    <div class="col-12">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-4">
                    <div class="card-body px-0 pt-1 pb-2">
                        <div class="container">
                            <h6>Data Count PKS</h6>
                            <canvas id="graphPKS" height="249" width="249"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-4">
                    <div class="card-body px-0 pt-1 pb-2">
                        <div class="container">
                            <h6>Data Count Category</h6>
                            <canvas id="graphCategory" height="249" width="249"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-4">
                    <div class="card-body px-0 pt-1 pb-2">
                        <div class="container">
                            <h6>Data Count Owner</h6>
                            <canvas id="graphOwner" height="249" width="249"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card mb-4">
            <div class="row">
                <div class="col-6">
                    <div class="card-header pb-0">
                        <h6>Data Potency</h6>
                        <a href="javascript:void(0)" id="btnTambah" class="btn btn-primary">
                            <i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah</a>
                        <a href="javascript:void(0)" id="btnImport" class="btn btn-secondary">
                            <i class="fas fa-file"></i>&nbsp;&nbsp;Import</a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card-header d-flex justify-content-end pb-0">
                        <h6> </h6>
                        <a href="{{ asset('import_datas/Import_Data_Potency_Format.xlsx') }}" class="btn btn-success">
                            <i class="fas fa-download"></i>&nbsp;&nbsp;Download Draft Import</a>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-1 pb-2">
                <div class="container">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li><i class="mdi mdi-alert mr-1 mt-1 mb-3"></i>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="table-responsive pb-2">
                        <table id="tabel" class="table align-items-center mb-0">
                            <thead class="table table-dark">
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder ">No.</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site ID</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Colocation</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">System</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Building Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Category</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Util Category</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">PKS</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Contract</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Start Leased</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">End Leased</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Profitability</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Ebit Margin</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Macro</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Lampsite</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Owner</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Desa</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Kecamatan</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Kabupaten</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Provinsi</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">ABD</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($dp as $data)
                                <tr>
                                    <td><span class="text-xs font-weight-bold">{{$no++}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->site_id }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->site_name }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->colocation }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->system }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->building_name }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->category }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->util_category }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->pks }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->contract }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->start_leased }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->end_leased }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->profitability }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->ebit_margin }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->macro }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->lampsite}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->owner}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->desa}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->kec}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->kab}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->prov}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->abd}}</span></td>
                                    <td class="align-middle">
                                        <a href="javascript:void(0)" class="btn btn-info mb-0" id="btnDetail" data-toggle="modal" data-id="{{ $data->id }} "><i class="fas fa-info me-2"></i> Detail</a>
                                        <a href="javascript:void(0)" class="btn btn-warning mb-0" id="btnEdit" data-toggle="modal" data-id="{{ $data->id }}"><i class="fas fa-pencil-alt me-2" aria-hidden="true"></i>Edit</a>
                                        <meta name=" csrf-token" content="{{ csrf_token() }}">
                                        <a href="javascript:void(0)" class="btn btn-danger mb-0" id="btnHapus" data-toggle="modal" data-id="{{ $data->id }}"><i class="far fa-trash-alt me-2"></i> Hapus</a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade bd-example-modal-lg" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambahLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahLabel"></h5>
            </div>
            <form action="{{route('dpTambah')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="action" id="action" value="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Site ID</label>
                                <input type="text" id="site_id" name="site_id" placeholder="Masukkan Site ID" class="form-control" onchange="validateID()" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Site Name</label>
                                <input type="text" id="site_name" name="site_name" placeholder="Masukkan Site Name" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Colocation</label>
                                <select name="colocation" id="colocation" class="form-control">
                                    <option value="2G">2G</option>
                                    <option value="2G 3G">2G 3G</option>
                                    <option value="2G 3G 4G">2G 3G 4G</option>
                                    <option value="2G 4G">2G 4G</option>
                                    <option value="3G 4G">3G 4G</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Building Name</label>
                                <input type="text" id="building_name" name="building_name" placeholder="Masukkan Building Name" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Category</label>
                                <select name="category" id="category" class="form-control">
                                    <option value="Airport">Airport</option>
                                    <option value="Apartment">Apartment</option>
                                    <option value="Campus">Campus</option>
                                    <option value="Government">Government</option>
                                    <option value="Hospital">Hospital</option>
                                    <option value="Hotel">Hotel</option>
                                    <option value="Office">Office</option>
                                    <option value="Shopping">Shopping</option>
                                    <option value="Center">Center</option>
                                    <option value="Venue">Venue</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Util Category</label>
                                <select name="util_category" id="util_category" class="form-control">
                                    <option value="RCI">RCI</option>
                                    <option value="High Util">High Util</option>
                                    <option value="Green">Green</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">PKS</label>
                                <select name="pks" id="pks" class="form-control">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Contract</label>
                                <select name="contract" id="contract" class="form-control">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Start Leased</label>
                                <input type="date" name="start_leased" id="start_leased" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">End Leased</label>
                                <input type="date" name="end_leased" id="end_leased" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Profitability</label>
                                <input type="text" name="profitability" id="profitability" placeholder="Masukkan Profitability" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Ebit Margin</label>
                                <input type="text" name="ebit_margin" id="ebit_margin" placeholder="Masukkan Ebit Margin" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">System</label>
                                <select name="system[]" id="system" class="js-example-basic-multiple" multiple="multiple" style="width: 100%;">
                                    <option value="G900">G900</option>
                                    <option value="D1800">D1800</option>
                                    <option value="U900">U900</option>
                                    <option value="U2100">U2100</option>
                                    <option value="L900">L900</option>
                                    <option value="L1800">L1800</option>
                                    <option value="L2100">L2100</option>
                                    <option value="L2300">L1800</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="s" class=" form-control-label">Macro</label>
                                <select id="macro" name="macro" class="form-control">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Lampsite</label>
                                <select name="lampsite" id="lampsite" class="form-control">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="s" class=" form-control-label">ABD</label>
                                <select id="abd" name="abd" class="form-control">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Owner</label>
                                <input type="text" id="owner" name="owner" placeholder="Masukkan Owner" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Desa</label>
                                <input class="form-control" list="datalistDesa" id="desa" name="desa" placeholder="Type to search...">
                                <datalist id="datalistDesa">
                                    @foreach($desa as $data)
                                    <option value="{{$data->desa}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Kecamatan</label>
                                <input class="form-control" list="datalistKec" id="kec" name="kec" placeholder="Type to search...">
                                <datalist id="datalistKec">
                                    @foreach($kec as $data)
                                    <option value="{{$data->kec}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Kabupaten</label>
                                <input class="form-control" list="datalistKab" id="kab" name="kab" placeholder="Type to search...">
                                <datalist id="datalistKab">
                                    @foreach($kab as $data)
                                    <option value="{{$data->kab}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Provinsi</label>
                                <input class="form-control" list="datalistProv" id="prov" name="prov" placeholder="Type to search...">
                                <datalist id="datalistProv">
                                    @foreach($prov as $data)
                                    <option value="{{$data->prov}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal tambah & edit -->
<!-- modal detail -->
<div class="modal fade bd-example-modal-lg" id="detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailLabel"></h5>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="table-detail">
                    <tbody>
                        <tr>
                            <th scope="col">Site ID</th>
                            <td width="4">:</td>
                            <td id="site_id1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Site Name</th>
                            <td width="4">:</td>
                            <td id="site_name1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Desa</th>
                            <td width="4">:</td>
                            <td id="desa1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Kecamatan</th>
                            <td width="4">:</td>
                            <td id="kec1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Kabupaten</th>
                            <td width="4">:</td>
                            <td id="kab1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Provinsi</th>
                            <td width="4">:</td>
                            <td id="prov1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Colocation</th>
                            <td width="4">:</td>
                            <td id="colocation1"></td>
                        </tr>
                        <tr>
                            <th scope="col">System</th>
                            <td width="4">:</td>
                            <td id="system1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Building Name</th>
                            <td width="4">:</td>
                            <td id="building_name1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Category</th>
                            <td width="4">:</td>
                            <td id="category1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Util Category</th>
                            <td width="4">:</td>
                            <td id="util_category1"></td>
                        </tr>
                        <tr>
                            <th scope="col">PKS</th>
                            <td width="4">:</td>
                            <td id="pks1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Contract</th>
                            <td width="4">:</td>
                            <td id="contract1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Start Leased</th>
                            <td width="4">:</td>
                            <td id="start_leased1"></td>
                        </tr>
                        <tr>
                            <th scope="col">End Leased</th>
                            <td width="4">:</td>
                            <td id="end_leased1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Profitability</th>
                            <td width="4">:</td>
                            <td id="profitability1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Ebit Margin</th>
                            <td width="4">:</td>
                            <td id="ebit_margin1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Macro</th>
                            <td width="4">:</td>
                            <td id="macro1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Lampsite</th>
                            <td width="4">:</td>
                            <td id="lampsite1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Owner</th>
                            <td width="4">:</td>
                            <td id="owner1"></td>
                        </tr>
                        <tr>
                            <th scope="col">ABD</th>
                            <td width="4">:</td>
                            <td id="abd1"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- modal import -->
<div class="modal fade bd-example-modal-lg" id="import" tabindex="-1" role="dialog" aria-labelledby="importLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importLabel"></h5>
            </div>
            <form action="{{route('dpImport')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Pilih File (*excel)</label>
                                <input type="file" id="file" name="file" placeholder="Masukkan File" class="form-control" accept="application/vnd.ms-excel, .xlsx">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal hapus -->
<div class="modal fade bd-example-modal-lg" id="hapus" tabindex="-1" role="dialog" aria-labelledby="hapusLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hapusLabel"></h5>
            </div>
            <form action="{{route('dpHapus')}}" method="POST">
                @csrf
                <input type="hidden" name="id1" id="id1">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ya</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    function validateID() {
        var site_id = document.getElementById("site_id").value;
        $.post('{{ route("dpCek") }}', {
            _token: '{{csrf_token()}}',
            site_id: site_id
        }, function(data) {
            if (data.length == 0) {
                return true;
            } else {
                swal("Error", "Site ID Sudah Ada!", "error");
                $('#site_id').val('');
            }
        });
    }

    $(document).ready(function() {
        $('#tabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv'
            ]
        });

        // modal detail
        $('body').on('click', '#btnDetail', function() {
            var data_id = $(this).data('id');
            $.get('data-potency/' + data_id + '/edit/', function(data) {
                $('#detailLabel').html("Detail Data Potency");
                $('#detail').modal('show');
                $('#site_id1').html(data.site_id);
                $('#site_name1').html(data.site_name);
                $('#desa1').html(data.desa);
                $('#kec1').html(data.kec);
                $('#kab1').html(data.kab);
                $('#prov1').html(data.prov);
                $('#colocation1').html(data.colocation);
                $('#system1').html(data.system);
                $('#building_name1').html(data.building_name);
                $('#category1').html(data.category);
                $('#util_category1').html(data.util_category);
                $('#pks1').html(data.pks);
                $('#contract1').html(data.contract);
                $('#start_leased1').html(data.start_leased);
                $('#end_leased1').html(data.end_leased);
                $('#profitability1').html(data.profitability);
                $('#ebit_margin1').html(data.ebit_margin);
                $('#macro1').html(data.macro);
                $('#lampsite1').html(data.lampsite);
                $('#owner1').html(data.owner);
                $('#abd1').html(data.abd);
            })
        });

        // modal tambah
        $(document).on('click', '#btnTambah', function(e) {
            $('#tambahLabel').html("Tambah Data Potency");
            $('#tambah').modal('show');
            $('input[name=action]').val('tambah');
            $('#id').val("");
            $('#site_id').val("");
            $('#site_name').val("");
            $('#owner').val("");
            $('#desa').val("");
            $('#kec').val("");
            $('#kab').val("");
            $('#prov').val("");
        });

        // modal import
        $(document).on('click', '#btnImport', function(e) {
            $('#importLabel').html("Import Data Potency");
            $('#import').modal('show');
            $('#file').val("");
        });

        // modal edit
        $('body').on('click', '#btnEdit', function() {
            var data_id = $(this).data('id');
            $.get('data-potency/' + data_id + '/edit', function(data) {
                $('#tambahLabel').html("Edit Data Potency");
                $('#btn-save').prop('disabled', false);
                $('#tambah').modal('show');
                $('input[name=action]').val('edit');
                $('#id').val(data.id);
                $('#site_id').val(data.site_id);
                $('#site_name').val(data.site_name);
                $('#colocation').val(data.colocation);
                var system = data.system.split(' ');
                $("#system").select2().val(system).trigger('change.select2')
                $('#building_name').val(data.building_name);
                $('#category').val(data.category);
                $('#util_category').val(data.util_category);
                $('#pks').val(data.pks);
                $('#contract').val(data.contract);
                $('#start_leased').val(data.start_leased);
                $('#end_leased').val(data.end_leased);
                $('#profitability').val(data.profitability);
                $('#ebit_margin').val(data.ebit_margin);
                $('#macro').val(data.macro);
                $('#lampsite').val(data.lampsite);
                $('#owner').val(data.owner);
                $('#desa').val(data.desa);
                $('#kec').val(data.kec);
                $('#kab').val(data.kab);
                $('#prov').val(data.prov);
                $('#abd').val(data.abd);
            })
        });

        // modal hapus
        $('body').on('click', '#btnHapus', function() {
            var data_id = $(this).data('id');
            $.get('data-potency/' + data_id + '/edit', function(data) {
                $('#hapusLabel').html("Hapus Data Potency");
                $('#btn-save').prop('disabled', false);
                $('#hapus').modal('show');
                $('#id1').val(data.id);
            })
        });
    });

    // chart
    $(function() {

        $.ajax({
            type: "POST",
            url: "{{ route('graphPKS') }}",
            data: {
                _token: "{{ csrf_token() }}",
            },
            success: function(response) {
                var label = response.data.map(function(e) {
                    return e.pks
                })

                var dt = response.data.map(function(e) {
                    return e.total
                })
                var doughnutPieData = {
                    datasets: [{
                        data: dt,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.5)',
                            'rgba(54, 162, 235, 0.5)',
                            'rgba(255, 206, 86, 0.5)',
                            'rgba(75, 192, 192, 0.5)',
                            'rgba(153, 102, 255, 0.5)',
                            'rgba(255, 159, 64, 0.5)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                    }],

                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: label
                };
                var doughnutPieOptions = {
                    responsive: true,
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                };

                var graphPKS = $("#graphPKS").get(0).getContext("2d");
                var pieChart = new Chart(graphPKS, {
                    type: 'pie',
                    data: doughnutPieData,
                    options: doughnutPieOptions
                });
            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route('graphCategory') }}",
            data: {
                _token: "{{ csrf_token() }}",
            },
            success: function(response) {
                var labels = response.data.map(function(e) {
                    return e.category
                })

                var dt = response.data.map(function(e) {
                    return e.total
                })

                var data = {
                    labels: labels,
                    datasets: [{
                        label: "",
                        data: dt,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1,
                        fill: false
                    }]
                };

                var options = {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {
                        display: false
                    },
                    elements: {
                        point: {
                            radius: 0
                        }
                    }

                };
                var graphCategory = $("#graphCategory").get(0).getContext("2d");
                // This will get the first returned node in the jQuery collection.
                var barChart = new Chart(graphCategory, {
                    type: 'bar',
                    data: data,
                    options: options
                });
            },
            error: function(xhr) {

            }
        });
        $.ajax({
            type: "POST",
            url: "{{ route('graphOwner') }}",
            data: {
                _token: "{{ csrf_token() }}",
            },
            success: function(response) {
                var labels = response.data.map(function(e) {
                    return e.owner
                })

                var dt = response.data.map(function(e) {
                    return e.total
                })

                var data = {
                    labels: labels,
                    datasets: [{
                        label: "",
                        data: dt,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1,
                        fill: false
                    }]
                };

                var options = {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {
                        display: false
                    },
                    elements: {
                        point: {
                            radius: 0
                        }
                    }

                };
                var graphOwner = $("#graphOwner").get(0).getContext("2d");
                // This will get the first returned node in the jQuery collection.
                var barChart = new Chart(graphOwner, {
                    type: 'bar',
                    data: data,
                    options: options
                });
            },
            error: function(xhr) {

            }
        });

    });
</script>
@endpush