@extends('layouts.master')
@section('content')
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Data Hardware</li>
        </ol>
    </nav>
    <div class="col-12">
        <div class="card mb-4">
            <div class="row">
                <div class="col-6">
                    <div class="card-header pb-0">
                        <h6>Data Request Hardware</h6>
                        <a href="javascript:void(0)" id="btnTambahReq" class="btn btn-primary">
                            <i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah</a>
                        <a href="javascript:void(0)" id="btnImportReq" class="btn btn-secondary">
                            <i class="fas fa-file"></i>&nbsp;&nbsp;Import</a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card-header pb-0" style="float: right;">
                        <h6> </h6>
                        <a href="{{ asset('import_datas/Import_Hardware_Req_Format.xlsx') }}" class="btn btn-success">
                            <i class="fas fa-download"></i>&nbsp;&nbsp;Download Draft Import</a>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-1 pb-2">
                <div class="container">
                    <div class="table-responsive pb-2">
                        <table id="tabel-1" class="table align-items-center mb-0">
                            <thead class="table table-dark">
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder ">No.</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Modul Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Type</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site ID</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Status</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($request as $data)
                                <tr>
                                    <td><span class="text-xs font-weight-bold">{{$no++}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->modul_name }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->type }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->site_id}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->status}}</span></td>
                                    <td class="align-middle">
                                        @if($data->status=="Waiting")
                                        <a href="javascript:void(0)" class="btn btn-info mb-0" id="btnApproved" data-toggle="modal" data-id="{{ $data->id }} "><i class="fas fa-verify me-2"></i> Approved</a>
                                        @endif
                                        <a href="javascript:void(0)" class="btn btn-warning mb-0" id="btnEditReq" data-toggle="modal" data-id="{{ $data->id }}"><i class="fas fa-pencil-alt me-2" aria-hidden="true"></i>Edit</a>
                                        <meta name=" csrf-token" content="{{ csrf_token() }}">
                                        <a href="javascript:void(0)" class="btn btn-danger mb-0" id="btnHapusReq" data-toggle="modal" data-id="{{ $data->id }}"><i class="far fa-trash-alt me-2"></i> Hapus</a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card mb-4">
            <div class="row">
                <div class="col-6">
                    <div class="card-header pb-0">
                        <h6>Data Hardware</h6>
                        <a href="javascript:void(0)" id="btnTambah" class="btn btn-primary">
                            <i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah</a>
                        <a href="javascript:void(0)" id="btnImport" class="btn btn-secondary">
                            <i class="fas fa-file"></i>&nbsp;&nbsp;Import</a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card-header pb-0" style="float: right;">
                        <h6> </h6>
                        <a href="{{ asset('import_datas/Import_Hardware_Format.xlsx') }}" class="btn btn-success">
                            <i class="fas fa-download"></i>&nbsp;&nbsp;Download Draft Import</a>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-1 pb-2">
                <div class="container">
                    <div class="table-responsive pb-2">
                        <table id="tabel-2" class="table align-items-center mb-0">
                            <thead class="table table-dark">
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder ">No.</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Modul Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Type</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Vendor</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Qty</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Unit</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Status Eqp</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site ID Donor</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site Name Donor</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site ID Acceptor</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Requestor</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Objective</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Status</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($hardware as $data)
                                <tr>
                                    <td><span class="text-xs font-weight-bold">{{$no++}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->modul_name }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->type }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->vendor }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->qty }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->unit}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->status_eqp}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->site_id_donor}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->site_name_donor}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->site_id_acceptor}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->requestor}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->objective}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->status}}</span></td>
                                    <td class="align-middle">
                                        <a href="javascript:void(0)" class="btn btn-info mb-0" id="btnDetail" data-toggle="modal" data-id="{{ $data->id }} "><i class="fas fa-info me-2"></i> Detail</a>
                                        <a href="javascript:void(0)" class="btn btn-warning mb-0" id="btnEdit" data-toggle="modal" data-id="{{ $data->id }}"><i class="fas fa-pencil-alt me-2" aria-hidden="true"></i>Edit</a>
                                        <meta name=" csrf-token" content="{{ csrf_token() }}">
                                        <a href="javascript:void(0)" class="btn btn-danger mb-0" id="btnHapus" data-toggle="modal" data-id="{{ $data->id }}"><i class="far fa-trash-alt me-2"></i> Hapus</a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade bd-example-modal-lg" id="tambahReq" tabindex="-1" role="dialog" aria-labelledby="tambahReqLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahReqLabel"></h5>
            </div>
            <form action="{{route('hardwareTambahReq')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="idReq" id="idReq">
                <input type="hidden" name="actionReq" id="actionReq" value="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Modul Name</label>
                                <input type="text" id="modul_nameReq" name="modul_nameReq" placeholder="Masukkan Modul Name" class="form-control" onchange="validateNama()" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Type</label>
                                <input type="text" id="typeReq" name="typeReq" placeholder="Masukkan Type" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hp" class=" form-control-label">Site ID</label>
                                <input type="text" id="site_idReq" name="site_idReq" placeholder="Masukkan Site ID" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambahLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahLabel"></h5>
            </div>
            <form action="{{route('hardwareTambah')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="action" id="action" value="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Modul Name</label>
                                <input type="text" id="modul_name" name="modul_name" placeholder="Masukkan Modul Name" class="form-control" onchange="validateNama()" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Type</label>
                                <input type="text" id="type" name="type" placeholder="Masukkan Type" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Vendor</label>
                                <input type="text" id="vendor" name="vendor" placeholder="Masukkan Vendor" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Qty</label>
                                <input type="number" id="qty" name="qty" placeholder="Masukkan Qty" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Unit</label>
                                <input type="text" id="unit" name="unit" placeholder="Masukkan Unit" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Status Eqp</label>
                                <input type="text" id="status_eqp" name="status_eqp" placeholder="Masukkan Status Eqp" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hp" class=" form-control-label">Site ID Donor</label>
                                <input type="text" id="site_id_donor" name="site_id_donor" placeholder="Masukkan Site ID Donor" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hp" class=" form-control-label">Site Name Donor</label>
                                <input type="text" id="site_name_donor" name="site_name_donor" placeholder="Masukkan Site Name Donor" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hp" class=" form-control-label">Site ID Acceptor</label>
                                <input type="text" id="site_id_acceptor" name="site_id_acceptor" placeholder="Masukkan Site ID Acceptor" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hp" class=" form-control-label">Requestor</label>
                                <input type="text" id="requestor" name="requestor" placeholder="Masukkan Requestor" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hp" class=" form-control-label">Objective</label>
                                <input type="text" id="objective" name="objective" placeholder="Masukkan Site ID Acceptor" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hp" class=" form-control-label">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="Approved">Approved</option>
                                    <option value="Rejected">Rejected</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal tambah & edit -->
<!-- modal detail -->
<div class="modal fade bd-example-modal-lg" id="detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailLabel"></h5>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="table-detail">
                    <tbody>
                        <tr>
                            <th scope="col">Modul Name</th>
                            <td width="4">:</td>
                            <td id="modul_name1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Type</th>
                            <td width="4">:</td>
                            <td id="type1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Vendor</th>
                            <td width="4">:</td>
                            <td id="vendor1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Qty</th>
                            <td width="4">:</td>
                            <td id="qty1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Unit</th>
                            <td width="4">:</td>
                            <td id="unit1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Status Eqp</th>
                            <td width="4">:</td>
                            <td id="status_eqp1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Site ID Donor</th>
                            <td width="4">:</td>
                            <td id="site_id_donor1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Site Name Donor</th>
                            <td width="4">:</td>
                            <td id="site_name_donor1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Site ID Acceptor</th>
                            <td width="4">:</td>
                            <td id="site_id_acceptor1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Requestor</th>
                            <td width="4">:</td>
                            <td id="requestor1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Objective</th>
                            <td width="4">:</td>
                            <td id="objective1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Status</th>
                            <td width="4">:</td>
                            <td id="status1"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- modal import -->
<div class="modal fade bd-example-modal-lg" id="import" tabindex="-1" role="dialog" aria-labelledby="importLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importLabel"></h5>
            </div>
            <form action="{{route('hardwareImport')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Pilih File (*excel)</label>
                                <input type="file" id="file" name="file" placeholder="Masukkan File" class="form-control" accept="application/vnd.ms-excel,.xlsx">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="importReq" tabindex="-1" role="dialog" aria-labelledby="importReqLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importReqLabel"></h5>
            </div>
            <form action="{{route('hardwareImportReq')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Pilih File (*excel)</label>
                                <input type="file" id="fileReq" name="fileReq" placeholder="Masukkan File" class="form-control" accept="application/vnd.ms-excel,.xlsx">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal hapus -->
<div class="modal fade bd-example-modal-lg" id="hapus" tabindex="-1" role="dialog" aria-labelledby="hapusLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hapusLabel"></h5>
            </div>
            <form action="{{route('hardwareHapus')}}" method="POST">
                @csrf
                <input type="hidden" name="id1" id="id1">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ya</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="hapusReq" tabindex="-1" role="dialog" aria-labelledby="hapusReqLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hapusReqLabel"></h5>
            </div>
            <form action="{{route('hardwareHapusReq')}}" method="POST">
                @csrf
                <input type="hidden" name="id1Req" id="id1Req">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ya</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    function validateNama() {
        var modul_name = document.getElementById("modul_name").value;
        $.post('{{ route("hardwareCek") }}', {
            _token: '{{csrf_token()}}',
            modul_name: modul_name
        }, function(data) {
            if (data.length == 0) {
                return true;
            } else {
                swal("Error", "Modul Name Sudah Ada!", "error");
                $('#modul_name').val('');
            }
        });
    }

    $(document).ready(function() {
        $('#tabel-1').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv'
            ]
        });
        $('#tabel-2').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv'
            ]
        });

        // modal detail
        $('body').on('click', '#btnDetail', function() {
            var data_id = $(this).data('id');
            $.get('hardware/' + data_id + '/edit/', function(data) {
                $('#detailLabel').html("Detail Data Hardware");
                $('#detail').modal('show');
                $('#modul_name1').html(data.modul_name);
                $('#type1').html(data.type);
                $('#vendor1').html(data.vendor);
                $('#qty1').html(data.qty);
                $('#unit1').html(data.unit);
                $('#status_eqp1').html(data.status_eqp);
                $('#site_id_donor1').html(data.site_id_donor);
                $('#site_name_donor1').html(data.site_name_donor);
                $('#site_id_acceptor1').html(data.site_id_acceptor);
                $('#requestor1').html(data.requestor);
                $('#objective1').html(data.objective);
                $('#status1').html(data.status);
            })
        });

        // modal tambah
        $(document).on('click', '#btnTambah', function(e) {
            $('#tambahLabel').html("Tambah Data Hardware");
            $('#tambah').modal('show');
            $('input[name=action]').val('tambah');
            $('#id').val("");
            $('#type').val("");
            $('#modul_name').val("");
            $('#vendor').val("");
            $('#qty').val("");
            $('#unit').val("");
            $('#site_id_donor').val("");
            $('#status_eqp').val("");
            $('#site_name_donor').val("");
            $('#site_id_acceptor').val("");
            $('#requestor').val("");
            $('#objective').val("");
        });
        $(document).on('click', '#btnTambahReq', function(e) {
            $('#tambahReqLabel').html("Tambah Data Request Hardware");
            $('#tambahReq').modal('show');
            $('input[name=actionReq]').val('tambah');
            $('#idReq').val("");
            $('#typeReq').val("");
            $('#modul_nameReq').val("");
            $('#site_idReq').val("");
        });

        // modal import
        $(document).on('click', '#btnImport', function(e) {
            $('#importLabel').html("Import Data Hardware");
            $('#import').modal('show');
            $('#file').val("");
        });
        $(document).on('click', '#btnImportReq', function(e) {
            $('#importReqLabel').html("Import Data Hardware");
            $('#importReq').modal('show');
            $('#fileReq').val("");
        });

        // modal edit
        $('body').on('click', '#btnEdit', function() {
            var data_id = $(this).data('id');
            $.get('hardware/' + data_id + '/edit', function(data) {
                $('#tambahLabel').html("Edit Data Hardware");
                $('#btn-save').prop('disabled', false);
                $('#tambah').modal('show');
                $('input[name=action]').val('edit');
                $('#id').val(data.id);
                $('#type').val(data.type);
                $('#modul_name').val(data.modul_name);
                $('#vendor').val(data.vendor);
                $('#qty').val(data.qty);
                $('#unit').val(data.unit);
                $('#status_eqp').val(data.status_eqp);
                $('#site_id_donor').val(data.site_id_donor);
                $('#site_name_donor').val(data.site_name_donor);
                $('#site_id_acceptor').val(data.site_id_acceptor);
                $('#requestor').val(data.requestor);
                $('#objective').val(data.objective);
                $('#status').val(data.status);
            })
        });
        $('body').on('click', '#btnEditReq', function() {
            var data_id = $(this).data('id');
            $.get('hardwareReq/' + data_id + '/edit', function(data) {
                $('#tambahReqLabel').html("Edit Data Request Hardware");
                $('#btn-save').prop('disabled', false);
                $('#tambahReq').modal('show');
                $('input[name=actionReq]').val('edit');
                $('#idReq').val(data.id);
                $('#typeReq').val(data.type);
                $('#modul_nameReq').val(data.modul_name);
                $('#site_idReq').val(data.site_id);
            })
        });

        // modal hapus
        $('body').on('click', '#btnHapus', function() {
            var data_id = $(this).data('id');
            $.get('hardware/' + data_id + '/edit', function(data) {
                $('#hapusLabel').html("Hapus Data Hardware");
                $('#btn-save').prop('disabled', false);
                $('#hapus').modal('show');
                $('#id1').val(data.id);
            })
        });
        $('body').on('click', '#btnHapusReq', function() {
            var data_id = $(this).data('id');
            $.get('hardwareReq/' + data_id + '/edit', function(data) {
                $('#hapusReqLabel').html("Hapus Data Request Hardware");
                $('#btn-save').prop('disabled', false);
                $('#hapusReq').modal('show');
                $('#id1Req').val(data.id);
            })
        });
    });
</script>
@endpush