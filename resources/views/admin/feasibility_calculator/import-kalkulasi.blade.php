@extends('layouts.master')
@section('content')
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="http://127.0.0.1:8000/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Feasibility</li>
        </ol>
    </nav>
    <div class="card card-body">
        <div>
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalImport">
                Import Data
            </button>
            <a href="{{ asset('import_datas/Import_Feasibility_Format.xlsx') }}" class="btn btn-info">
                Download Format Import
            </a>
            <button id="btn-download" type="button" class="btn btn-success hide" onClick="tableToCSV()">
                Download CSV
            </button>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td>NO</td>
                            <td>CAPEX EQP</td>
                            <td>TSA EQP</td>
                            <td>CAPEX CME</td>
                            <td>CAPEX Depreciation (Month/5 Years)</td>
                            <td>TOTAL CAPEX</td>
                            <td>OPEX NSR/Month</td>
                            <td>OPEX Power/Month</td>
                            <td>OPEX O & M/Month</td>
                            <td>OPEX Transmisi/Month</td>
                            <td>OPEX Frekuensi/Month</td>
                            <td>TOTAL OPEX</td>
                            <td>AVERAGE REV / Month</td>
                            <td>PROFITABILITY</td>
                            <td>EBITDA Margin</td>
                            <td>PROFITABILITY W INVESTEMENT</td>
                            <td>EBIT MARGIN</td>
                            <td>AVERAGE REV / Month Min</td>
                            <td>EBIT</td>
                        </tr>
                    </thead>
                    <tbody id="loadData">
                        <tr>
                            <td id="no_data" colspan="18">
                                Tidak ada data....
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modalImport" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Import Data</h5>
                <button type="button" class="btn-close text-secondary" data-bs-dismiss="modal" aria-label="Close">X</button>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" id="importFeasibility">
                    <div class="form-group">
                        <label for="formFile" class="form-label">Ambil file excel anda</label>
                        <input class="form-control" type="file" name="fileExcel" id="fileExcel" />
                    </div>
                    <button class="btn btn-success" type="submit">Upload</button>
                </form>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function (e) {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#importFeasibility').submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type:'POST',
                url: "{{ url('proses-import_feasibility')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    if(data.data.length > 0){
                        document.getElementById("btn-download").classList.remove("hide");
                        document.getElementById("no_data").classList.add("hide");
                    }
                    $("#modalImport").modal("hide");
                    this.reset();
                    var content = "";
                    data.data.forEach(row => {
                        content += "<tr>"+
                            "<td>"+row.no+"</td>"+
                            "<td>"+row.capexEQP+"</td>"+
                            "<td>"+row.tsaEQP+"</td>"+
                            "<td>"+row.capexCME+"</td>"+
                            "<td>"+row.capexDep+"</td>"+
                            "<td>"+row.totalCapex+"</td>"+
                            "<td>"+row.opexNSR+"</td>"+
                            "<td>"+row.opexPower+"</td>"+
                            "<td>"+row.opexOM+"</td>"+
                            "<td>"+row.opexTransmisi+"</td>"+
                            "<td>"+row.opexFrekuensi+"</td>"+
                            "<td>"+row.totalOpex+"</td>"+
                            "<td>"+row.averageREV+"</td>"+
                            "<td>"+row.profitability+"</td>"+
                            "<td>"+row.ebitdaMargin+"</td>"+
                            "<td>"+row.profitabilityInvestement+"</td>"+
                            "<td>"+row.ebitMargin+"</td>"+
                            "<td>"+row.averageREVMin+"</td>"+
                            "<td>"+row.ebit+"</td></tr>";
                    });
                    console.log(content);
                    $("#loadData").html(content);
                },
                error: function(data){
                    console.log(data);
                }
            });
        });
    });

    function tableToCSV() {
        var csv_data = [];
        var rows = document.getElementsByTagName('tr');
        for (var i = 0; i < rows.length; i++) {
            var cols = rows[i].querySelectorAll('td,th');
            var csvrow = [];
            for (var j = 0; j < cols.length; j++) {
                csvrow.push(cols[j].innerHTML);
            }
            csv_data.push(csvrow.join(","));
        }
        csv_data = csv_data.join('\n');
        downloadCSVFile(csv_data);
    }

    function downloadCSVFile(csv_data) {
        CSVFile = new Blob([csv_data], {
            type: "text/csv"
        });

        var temp_link = document.createElement('a');
        temp_link.download = "Hasil_Feasibility_Calculator.csv";
        var url = window.URL.createObjectURL(CSVFile);
        temp_link.href = url;

        temp_link.style.display = "none";
        document.body.appendChild(temp_link);

        temp_link.click();
        document.body.removeChild(temp_link);
    }

</script>
@endpush