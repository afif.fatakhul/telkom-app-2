@extends('layouts.master')
@section('content')
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Feasibility Calculator</li>
        </ol>
    </nav>
        <div class="card card-body">
            <form action="{{ url('feasibility_exportexcel') }}" method="GET">
                @csrf
                <table class="table table-bordered table-striped">
                    <thead>  
                        <tr>
                            <th>PROGRAM</th>
                            <th>SYSTEM</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th rowspan="5">CAPEX</th>
                            <th>CAPEX EQP</th>
                            <th>
                                <input type="text" id="capexEQP" name="capexEQP" class="form-control" />
                            </th>
                        </tr>
                        <tr>
                            <th>TSA EQP</th>
                            <th>
                                <input type="text" name="tsaEQP" id="tsaEQP" class="form-control" />
                            </th>
                        </tr>
                        <tr>
                            <th>CAPEX CME</th>
                            <th>
                                <input type="text" name="capexCME" id="capexCME" placeholder="Ketik ulang untuk menghitung...." class="form-control" />
                            </th>
                        </tr>
                        <tr>
                            <th>CAPEX Depreciation (Month/5 Years)</th>
                            <th>
                                <span id="capexDepreciation"></span>
                                <input name="capexDepreciation" type="hidden" id="capexDepreciation2" />
                            </th>
                        </tr>
                        <tr>
                            <th>TOTAL CAPEX</th>
                            <th>
                                <span id="totalCapex"></span>
                                <input name="totalCapex" type="hidden" id="totalCapex2" />
                            </th>
                        </tr>
                        <tr>
                            <th rowspan="6">OPEX</th>
                            <th>OPEX NSR/Month</th>
                            <th>
                                <input type="text" name="opexNSR" id="opexNSR" class="form-control" />
                            </th>
                        </tr>
                        <tr>
                            <th>OPEX POWER</th>
                            <th>
                                <input type="text" name="opexPower" id="opexPower" class="form-control" />
                            </th>
                        </tr>
                        <tr>
                            <th>OPEX O & M</th>
                            <th>
                                <input type="text" name="opexOM" id="opexOM" class="form-control" />
                            </th>
                        </tr>
                        <tr>
                            <th>OPEX Transmisi</th>
                            <th>
                                <input type="text" name="opexTransmisi" id="opexTransmisi" class="form-control" />
                            </th>
                        </tr>
                        <tr>
                            <th>OPEX Frekuensi</th>
                            <th>
                                <input type="text" name="opexFrekuensi" id="opexFrekuensi" placeholder="Ketik ulang untuk menghitung...." class="form-control" />
                            </th>
                        </tr>
                        <tr>
                            <th>TOTAL OPEX</th>
                            <th>
                                <span id="totalOpex"></span>
                                <input type="hidden" name="totalOpex" id="totalOpex2" />
                            </th>
                        </tr>
                        <tr>
                            <th>REVENUE</th>
                            <th>AVERAGE REVENUE/Month</th>
                            <td>
                                <input type="text" name="avgRevenue" id="avgRevenue" class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <th rowspan="4">PROFITABILITY</th>
                            <th>Profitability</th>
                            <th>
                                <span id="profitability"></span>
                                <input type="hidden" name="profitability" id="profitability2" />
                            </th>
                        </tr>
                        <tr>
                            <th>EBITDA Margin</th>
                            <th>
                                <span id="ebitda"></span>
                                <input type="hidden" name="ebitda" id="ebitda2" />
                            </th>
                        </tr>
                        <tr>
                            <th>Profitability w Investement</th>
                            <th>
                                <span id="profitabilityWI"></span>
                                <input type="hidden" name="profitabilityWI" id="profitabilityWI2" />
                            </th>
                        </tr>
                        <tr>
                            <th>Ebit Margin</th>
                            <th>
                                <span id="ebitMargin"></span>
                                <input type="hidden" name="ebitMargin" id="ebitMargin2" />
                            </th>
                        </tr>
                        <tr>
                            <th rowspan="2">REV TO EBIT 38%</th>
                            <th>AVERAGE REV/Month MIN</th>
                            <th>
                                <span id="avgRevMonth"></span>
                                <input type="hidden" name="avgRevMonth" id="avgRevMonth2" />
                            </th>
                        </tr>
                        <tr>
                            <th>EBIT</th>
                            <th>
                                <span id="ebit"></span>
                                <input type="hidden" name="ebit" id="ebit2" />
                            </th>
                        </tr>
                    </tbody>
                </table>
                <button type="submit" class="btn btn-success">Export Excel</button>
            </form>
        </div>
</div>
@endsection
@push('scripts')
    <script>
        const capexCME = document.querySelector("#capexCME");
        const capexEQP = document.querySelector("#capexEQP");
        const tsaEQP = document.querySelector("#tsaEQP");
        const capexDepreciation = document.querySelector("#capexDepreciation");
        const capexDepreciation2 = document.querySelector("#capexDepreciation2");
        const totalCapex = document.querySelector("#totalCapex");
        const totalCapex2 = document.querySelector("#totalCapex2");

        const opexNSR = document.querySelector("#opexNSR");
        const opexOM = document.querySelector("#opexOM");
        const opexPower = document.querySelector("#opexPower");
        const opexTransmisi = document.querySelector("#opexTransmisi");
        const opexFrekuensi = document.querySelector("#opexFrekuensi");
        const totalOpex = document.querySelector("#totalOpex");
        const totalOpex2 = document.querySelector("#totalOpex2");

        const avgRevenue = document.querySelector("#avgRevenue");
        var nTotalOpex = 0;

        const profitability = document.querySelector("#profitability");
        const profitability2 = document.querySelector("#profitability2");
        const ebitda = document.querySelector("#ebitda");
        const ebitda12 = document.querySelector("#ebitda2");
        const profitabilityWI = document.querySelector("#profitabilityWI");
        const profitabilityWI2 = document.querySelector("#profitabilityWI2");
        const ebitMargin = document.querySelector("#ebitMargin");
        const ebitMargin2 = document.querySelector("#ebitMargin2");

        const avgRevMonth = document.querySelector("#avgRevMonth");
        const avgRevMonth2 = document.querySelector("#avgRevMonth2");
        const ebit = document.querySelector("#ebit");
        const ebit2 = document.querySelector("#ebit2");

        capexCME.addEventListener("change", function(){
            var totalCapexDepreciation = (parseInt(capexCME.value) + parseInt(capexEQP.value) + parseInt(tsaEQP.value)) / 5 /12;
            capexDepreciation.innerHTML = totalCapexDepreciation;
            capexDepreciation2.value = totalCapexDepreciation;
            totalCapex.innerHTML = parseInt(capexCME.value) + parseInt(capexEQP.value) + parseInt(tsaEQP.value) + parseInt(totalCapexDepreciation);
            totalCapex2.value = parseInt(capexCME.value) + parseInt(capexEQP.value) + parseInt(tsaEQP.value);
        });

        opexFrekuensi.addEventListener("change", function(){
            nTotalOpex = parseInt(opexNSR.value) + parseInt(opexOM.value) + parseInt(opexPower.value) + parseInt(opexTransmisi.value) + parseInt(opexFrekuensi.value);
            totalOpex.innerHTML = nTotalOpex;
            totalOpex2.value = nTotalOpex;
        });

        avgRevenue.addEventListener("change", function(){
            profitability.innerHTML = parseInt(avgRevenue.value) - parseInt(nTotalOpex);
            profitability2.value = parseInt(avgRevenue.value) - parseInt(nTotalOpex);
            ebitda.innerHTML = parseInt(profitability.innerHTML) / parseInt(avgRevenue.value) * 100;
            ebitda2.value = parseInt(profitability.innerHTML) / parseInt(avgRevenue.value) * 100;
            profitabilityWI.innerHTML = parseInt(avgRevenue.value) - parseInt(nTotalOpex) - parseInt(capexDepreciation.innerHTML);
            profitabilityWI2.value = parseInt(avgRevenue.value) - parseInt(nTotalOpex) - parseInt( capexDepreciation.innerHTML);
            ebitMargin.innerHTML = parseInt(profitabilityWI.innerHTML) / parseInt(avgRevenue.value) * 100;
            ebitMargin2.value = parseInt(profitabilityWI.innerHTML) / parseInt(avgRevenue.value) * 100;
            avgRevMonth.innerHTML = (parseInt(nTotalOpex) - parseInt(capexDepreciation.innerHTML) - parseInt(tsaEQP.value)) / (1-0.38);
            ebit.innerHTML = 38/100 * parseInt(ebitMargin.innerHTML);
            avgRevMonth2.value = (parseInt(nTotalOpex) - parseInt(capexDepreciation.innerHTML) - parseInt(tsaEQP.value)) / (1-0.38);
            ebit2.value = 38/100 * parseInt(ebitMargin.innerHTML);
        }); 
    </script>
@endpush