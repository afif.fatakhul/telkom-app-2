<table>
    <thead>  
        <tr>
            <th>PROGRAM</th>
            <th>SYSTEM</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
           <th rowspan="5">CAPEX</th>
           <th>CAPEX EQP</th>
           <th>{{ $capexEQP }}</th>
        </tr>
        <tr>
            <th>TSA EQP</th>
            <th>{{ $tsaEQP }}</th>
        </tr>
        <tr>
            <th>CAPEX CME</th>
            <th>{{ $capexCME }}</th>
        </tr>
        <tr>
            <th>CAPEX Depreciation (Month/5 Years)</th>
            <th>{{ $capexDepreciation }}</th>
        </tr>
        <tr>
            <th>TOTAL CAPEX</th>
            <th>{{ $totalCapex }}</th>
        </tr>
        <tr>
            <th rowspan="6">OPEX</th>
            <th>OPEX NSR/Month</th>
            <th>{{ $opexNSR }}</th>
        </tr>
        <tr>
            <th>OPEX POWER</th>
            <th>{{ $opexPower }}</th>
        </tr>
        <tr>
            <th>OPEX O dan M</th>
            <th>{{ $opexOM }}</th>
        </tr>
        <tr>
            <th>OPEX Transmisi</th>
            <th>{{ $opexTransmisi }}</th>
        </tr>
        <tr>
            <th>OPEX Frekuensi</th>
            <th>{{ $opexFrekuensi }}</th>
        </tr>
        <tr>
            <th>TOTAL OPEX</th>
            <th>{{ $totalOpex }}</th>
        </tr>
       <tr>
            <th>REVENUE</th>
            <th>AVERAGE REVENUE/Month</th>
            <th>{{ $avgRevenue }}</th>
        </tr>
        <tr>
            <th rowspan="4">PROFITABILITY</th>
            <th>Profitability</th>
            <th>{{ $profitability }}</th>
        </tr>
        <tr>
            <th>EBITDA Margin</th>
            <th>{{ $ebitda }}</th>
        </tr>
        <tr>
            <th>Profitability w Investement</th>
            <th>{{ $profitabilityWI }}</th>
        </tr>
        <tr>
            <th>Ebit Margin</th>
            <th>{{ $ebitMargin }}</th>
        </tr>
        <tr>
            <th rowspan="2">REV TO EBIT 38%</th>
            <th>AVERAGE REV/Month MIN</th>
            <th>{{ $avgRevMonth }}</th>
        </tr>
        <tr>
            <th>EBIT</th>
            <th>{{ $ebit }}</th>
        </tr>
    </tbody>
</table>