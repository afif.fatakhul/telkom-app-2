@extends('layouts.master')
@section('content')
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Proposal</li>
        </ol>
    </nav>
    <div class="col-12">
        <div class="card mb-4">
            <div class="row">
                <div class="col-6">
                    <div class="card-header pb-0">
                        <h6>Proposal</h6>
                        <a href="javascript:void(0)" id="btnTambah" class="btn btn-primary">
                            <i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah</a>
                        <a href="javascript:void(0)" id="btnImport" class="btn btn-secondary">
                            <i class="fas fa-file"></i>&nbsp;&nbsp;Import</a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card-header pb-0" style="float: right;">
                        <h6> </h6>
                        <a href="{{ asset('import_datas/Import_Proposal_Format.xlsx') }}" class="btn btn-success">
                            <i class="fas fa-download"></i>&nbsp;&nbsp;Download Draft Import</a>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-1 pb-2">
                <div class="container">
                    <div class="table-responsive pb-2">
                        <table id="tabel" class="table align-items-center mb-0">
                            <thead class="table table-dark">
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder ">No.</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Nama User</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Divisi</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Longitude</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Latitude</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Desa</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Kecamatan</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Kabupaten</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Provinsi</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Revenue Commit.</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Market Share</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Objective</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Total Cust.</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Nodin</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($proposal as $data)
                                <tr>
                                    <td><span class="text-xs font-weight-bold">{{$no++}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->name }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->divisi }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->long }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->lat }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->desa }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->kec}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->kab}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->prov}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->revenue_commitment}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->market_share}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->objective}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{$data->total_customer}}</span></td>
                                    <td><span class="text-xs font-weight-bold"><a href="{{asset('file-nodin/'.$data->nodin)}}" target="_blank">{{$data->nodin}}</a></span></td>
                                    <td class=" align-middle">
                                        <a href="javascript:void(0)" class="btn btn-info mb-0" id="btnDetail" data-toggle="modal" data-id="{{ $data->id }} "><i class="fas fa-info me-2"></i> Detail</a>
                                        <a href="javascript:void(0)" class="btn btn-warning mb-0" id="btnEdit" data-toggle="modal" data-id="{{ $data->id }}"><i class="fas fa-pencil-alt me-2" aria-hidden="true"></i>Edit</a>
                                        <meta name=" csrf-token" content="{{ csrf_token() }}">
                                        <a href="javascript:void(0)" class="btn btn-danger mb-0" id="btnHapus" data-toggle="modal" data-id="{{ $data->id }}"><i class="far fa-trash-alt me-2"></i> Hapus</a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade bd-example-modal-lg" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambahLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahLabel"></h5>
            </div>
            <form action="{{route('pTambah')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="action" id="action" value="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Divisi</label>
                                <input type="text" id="divisi" name="divisi" placeholder="Masukkan Divisi" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">SoW</label>
                                <input type="text" id="sow" name="sow" placeholder="Masukkan SoW" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Longitude</label>
                                <input type="text" id="long" name="long" placeholder="Masukkan Longitude" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Latitude</label>
                                <input type="text" id="lat" name="lat" placeholder="Masukkan Latitude" class="form-control" onchange="validateNama()" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Desa</label>
                                <input class="form-control" list="datalistDesa" id="desa" name="desa" placeholder="Type to search...">
                                <datalist id="datalistDesa">
                                    @foreach($desa as $data)
                                    <option value="{{$data->desa}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Kecamatan</label>
                                <input class="form-control" list="datalistKec" id="kec" name="kec" placeholder="Type to search...">
                                <datalist id="datalistKec">
                                    @foreach($kec as $data)
                                    <option value="{{$data->kec}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Kabupaten</label>
                                <input class="form-control" list="datalistKab" id="kab" name="kab" placeholder="Type to search...">
                                <datalist id="datalistKab">
                                    @foreach($kab as $data)
                                    <option value="{{$data->kab}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Provinsi</label>
                                <input class="form-control" list="datalistProv" id="prov" name="prov" placeholder="Type to search...">
                                <datalist id="datalistProv">
                                    @foreach($prov as $data)
                                    <option value="{{$data->prov}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Revenue Commitment</label>
                                <input type="text" id="revenue_commitment" name="revenue_commitment" placeholder="Masukkan Revenue Commitment" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Market Share</label>
                                <input type="text" id="market_share" name="market_share" placeholder="Masukkan Market Share" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Objective</label>
                                <input type="text" id="objective" name="objective" placeholder="Masukkan Colocation" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Total Customer (Target)</label>
                                <input type="number" id="total_customer" name="total_customer" placeholder="Masukkan Total Customer" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Nodin <span class="text-xs text-danger font-weight-bold">(*pdf)</span></label>
                                <input type="file" id="nodin" name="nodin" class="form-control" accept=".pdf">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal tambah & edit -->
<!-- modal detail -->
<div class="modal fade bd-example-modal-lg" id="detail" tabindex="-1" role="dialog" aria-labelledby="detailLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailLabel"></h5>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="table-detail">
                    <tbody>
                        <tr>
                            <th scope="col">Nama User</th>
                            <td width="4">:</td>
                            <td id="name1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Divisi</th>
                            <td width="4">:</td>
                            <td id="divisi1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Sow</th>
                            <td width="4">:</td>
                            <td id="sow1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Longitude</th>
                            <td width="4">:</td>
                            <td id="long1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Latitude</th>
                            <td width="4">:</td>
                            <td id="lat1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Desa</th>
                            <td width="4">:</td>
                            <td id="desa1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Kecamatan</th>
                            <td width="4">:</td>
                            <td id="kec1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Kabupaten</th>
                            <td width="4">:</td>
                            <td id="kab1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Provinsi</th>
                            <td width="4">:</td>
                            <td id="prov1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Revenue Commitment</th>
                            <td width="4">:</td>
                            <td id="revenue_commitment1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Market Share</th>
                            <td width="4">:</td>
                            <td id="market_share1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Objective</th>
                            <td width="4">:</td>
                            <td id="objective1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Total Customer</th>
                            <td width="4">:</td>
                            <td id="total_customer1"></td>
                        </tr>
                        <tr>
                            <th scope="col">Nodin</th>
                            <td width="4">:</td>
                            <td id="nodin1"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- modal import -->
<div class="modal fade bd-example-modal-lg" id="import" tabindex="-1" role="dialog" aria-labelledby="importLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="importLabel"></h5>
            </div>
            <form action="{{route('pImport')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Pilih File (*excel)</label>
                                <input type="file" id="file" name="file" placeholder="Masukkan File" class="form-control" accept="application/vnd.ms-excel,.xlsx">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal hapus -->
<div class="modal fade bd-example-modal-lg" id="hapus" tabindex="-1" role="dialog" aria-labelledby="hapusLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hapusLabel"></h5>
            </div>
            <form action="{{route('pHapus')}}" method="POST">
                @csrf
                <input type="hidden" name="id1" id="id1">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ya</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#tabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv'
            ]
        });

        // modal detail
        $('body').on('click', '#btnDetail', function() {
            var data_id = $(this).data('id');
            $.get('proposal/' + data_id + '/edit/', function(data) {
                $('#detailLabel').html("Detail Data Potency");
                $('#detail').modal('show');
                $('#name1').html(data.name);
                $('#long1').html(data.long);
                $('#lat1').html(data.lat);
                $('#desa1').html(data.desa);
                $('#kec1').html(data.kec);
                $('#kab1').html(data.kab);
                $('#prov1').html(data.prov);
                $('#revenue_commitment1').html(data.revenue_commitment);
                $('#market_share1').html(data.market_share);
                $('#objective1').html(data.objective);
                $('#divisi1').html(data.divisi);
                $('#total_customer1').html(data.total_customer);
                $('#sow1').html(data.sow);
                if (data.nodin != null) {
                    $('#nodin1').html('<a href="file-nodin/' + data.nodin + '" target="_blank">' + data.nodin + '</a>');
                }
            })
        });

        // modal tambah
        $(document).on('click', '#btnTambah', function(e) {
            $('#tambahLabel').html("Tambah Data Potency");
            $('#tambah').modal('show');
            $('input[name=action]').val('tambah');
            $('#id').val("");
            $('#long').val("");
            $('#lat').val("");
            $('#revenue_commitment').val("");
            $('#market_share').val("");
            $('#objective').val("");
            $('#desa').val("");
            $('#kec').val("");
            $('#kab').val("");
            $('#prov').val("");
            $('#divisi').val("");
            $('#total_customer').val("");
            $('#nodin').val("");
            $('#sow').val("");
        });

        // modal import
        $(document).on('click', '#btnImport', function(e) {
            $('#importLabel').html("Import Data Proposal");
            $('#import').modal('show');
            $('#file').val("");
        });

        // modal edit
        $('body').on('click', '#btnEdit', function() {
            var data_id = $(this).data('id');
            $.get('proposal/' + data_id + '/edit', function(data) {
                $('#tambahLabel').html("Edit Data Proposal");
                $('#btn-save').prop('disabled', false);
                $('#tambah').modal('show');
                $('input[name=action]').val('edit');
                $('#id').val(data.id);
                $('#long').val(data.long);
                $('#lat').val(data.lat);
                $('#revenue_commitment').val(data.revenue_commitment);
                $('#market_share').val(data.market_share);
                $('#objective').val(data.objective);
                $('#desa').val(data.desa);
                $('#kec').val(data.kec);
                $('#kab').val(data.kab);
                $('#prov').val(data.prov);
                $('#divisi').val(data.divisi);
                $('#total_customer').val(data.total_customer);
                $('#nodin').val(data.nodin);
                $('#sow').val(data.sow);
            })
        });

        // modal hapus
        $('body').on('click', '#btnHapus', function() {
            var data_id = $(this).data('id');
            $.get('proposal/' + data_id + '/edit', function(data) {
                $('#hapusLabel').html("Hapus Data Proposal");
                $('#btn-save').prop('disabled', false);
                $('#hapus').modal('show');
                $('#id1').val(data.id);
            })
        });
    });
</script>
@endpush