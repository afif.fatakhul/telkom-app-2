@extends('layouts.master')
@section('content')
<div class="row">
    <h6 class="font-weight-bolder mb-3">Dashboard</h6>
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-capitalize font-weight-bold">Total Sites</p>
                            <h5 class="font-weight-bolder mb-0">
                                {{ $totalSite }}
                            </h5>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div class="icon icon-shape bg-gradient-info shadow text-center border-radius-md">
                            <span class="fa fa-broadcast-tower text-white mt-3 text-lg"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-capitalize font-weight-bold">LAMPSITE</p>
                            <h5 class="font-weight-bolder mb-0">
                                {{ $lampsite }}
                            </h5>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div class="icon icon-shape bg-gradient-info shadow text-center border-radius-md">
                           <span class="fa fa-chart-bar text-white mt-3 text-lg"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-capitalize font-weight-bold">INDOOR MACRO</p>
                            <h5 class="font-weight-bolder mb-0">
                                {{ $macro }}
                            </h5>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div class="icon icon-shape bg-gradient-info shadow text-center border-radius-md">
                            <span class="fa fa-chart-area text-white mt-3 text-lg"></span>                  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6">
        <div class="card">
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-8">
                        <div class="numbers">
                            <p class="text-sm mb-0 text-capitalize font-weight-bold">OWNER</p>
                            <h5 class="font-weight-bolder mb-0">
                                {{ $owner }}
                            </h5>
                        </div>
                    </div>
                    <div class="col-4 text-end">
                        <div class="icon icon-shape bg-gradient-info shadow text-center border-radius-md">
                            <span class="fa fa-sim-card text-white mt-3 text-lg"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-lg-12">
        <div class="card card-body">
            <div id="vmap" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
</div>
<div class="row my-4">
    <div class="col-lg-6 col-md-6 mb-md-0 mb-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Top 10 Payloads</h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site ID</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Payload</th>
                            </tr>
                        </thead>
                        <tbody id="loadTopPayload">
                            <!-- <tr>
                                <td>
                                    <div class="d-flex px-2 py-1">
                                        <div class="d-flex flex-column justify-content-center">
                                            <h6 class="mb-0 text-sm">Soft UI XD Version</h6>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle text-center text-sm">
                                    <span class="text-xs font-weight-bold"> $14,000 </span>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 mb-md-0 mb-4">
        <div class="card">
            <div class="card-header pb-0">
                <div class="row">
                    <div class="col-lg-6 col-7">
                        <h6>Bottom 10 Payloads</h6>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pb-2">
                <div class="table-responsive">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site ID</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Site Name</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Payload</th>
                            </tr>
                        </thead>
                        <tbody id="loadBottomPayload"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row my-4">
    <div class="col-md-6">
        <div class="card card-body">
            <h5>Ebit Margin Single</h5>
            <canvas height="300" class="ebitMarginSinggle chart mt-3"></canvas>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-body">
            <h5>Ebit Margin Multi</h5>
        </div>
    </div>
</div>
<div class="row my-4">
    <div class="col-md-6">
        <div class="card card-body">
            <h5>Building Type</h5>
            <canvas height="300" class="buildingType chart mt-3"></canvas>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-body">
            <h5>Revenue By Building</h5>
        </div>
    </div>
</div>
@endsection
@push('scripts')<script>
    jQuery(document).ready(function() {
        var activeNations = new Array('path11', 'path12', 'path14');
        jQuery('#vmap').vectorMap({
            map: 'indonesia_id',
            enableZoom: true,
            showTooltip: true,
            backgroundColor: 'transparent',
            color: '#cecdcd',
            colors: {
                'path11': '#ff8300',
                'path12': '#fc5050',
                'path14': '#5ad608',
            },
            selectedColor: null,
            onRegionClick: function(event, code, region) {
                event.preventDefault();
                console.log(code);
            },
            onLabelShow: function(event, label, code) {
                if (activeNations.indexOf(code) > -1) {
                    switch (code) {
                        case 'path11':
                            label.text('Jakarta 1');
                            break;
                        case 'path12':
                            label.text('Jabar 1');
                            break;
                        case 'path14':
                            label.text('Banten 1');
                            break;
                    }
                } else {
                    event.preventDefault();
                }
            }
        });
    });

    topPayload();
    function topPayload() {
        $.ajax({
            url: "{{ url('payload_potency') }}",
            type: "GET",
            data: {"sort":"desc", "limit":10},
            success: function(response){
                console.log(response.data);
                var contents = "";
                response.data.forEach(row => {
                    contents += "<tr><td>"+row.site_id+"</td>"+
                        "<td>"+row.site_name+"</td>"+
                        "<td>"+row.payload+"</td></tr>"
                });
                $("#loadTopPayload").html(contents);
            }
        })
    }

    bottomPayload();
    function bottomPayload() {
        $.ajax({
            url: "{{ url('payload_potency') }}",
            type: "GET",
            data: {"sort":"asc", "limit":10},
            success: function(response){
                console.log(response.data);
                var contents = "";
                response.data.forEach(row => {
                    contents += "<tr><td>"+row.site_id+"</td>"+
                        "<td>"+row.site_name+"</td>"+
                        "<td>"+row.payload+"</td></tr>"
                });
                $("#loadBottomPayload").html(contents);
            }
        });
    }
    
    loadEbitMarginSinggle();
    function loadEbitMarginSinggle() {
        $.ajax({
            url: "{{ url('ebit_single') }}",
            type: "GET",
            success: function(response){
                Chart.defaults.global.defaultFontSize = 14;
    
                var cEbitMargin = response.data;
                var ctx = $(".ebitMarginSinggle");

                var listEbitMarginSinggle = {
                    label: "Ebit Margin Singgle",
                    data: response.data,
                    backgroundColor: [
                        "#80dda7",
                        "#b5b5b5",
                        "#e3e567",
                        "#f9b06b",
                        "#f46e6e"
                    ],
                };

                var chartOptions = {
                    legend: {
                        display: true,
                        labels: {
                            fontSize: 16,
                        },
                    },
                    plugins: {
                        datalabels: {
                            formatter: (value, ctx) => {
                                let datasets = ctx.chart.data.datasets;
                                if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
                                    let sum = datasets[0].data.reduce((a, b) => a + b, 0);
                                    let percentage = Math.round((value / sum) * 100) + "%";
                                    return percentage;
                                } else {
                                    return percentage;
                                }
                            },
                            labels: {
                                title: {
                                    font: {
                                        size: 16,
                                    },
                                },
                            },
                        },
                    },
                };

                new Chart(ctx, {
                    type: "pie",
                    data: {
                        labels: response.label,
                        datasets: [listEbitMarginSinggle],
                    },
                    options: chartOptions,
                });
            }
        });

    }

    loadBuildingType();
    function loadBuildingType() {
        $.ajax({
            url: "{{ url('building_type') }}",
            type: "GET",
            success: function(response){
                Chart.defaults.global.defaultFontSize = 14;
    
                var cBuildingType = response.data;
                var ctx = $(".buildingType");

                var listBuildingType = {
                    label: "Building Type",
                    data: cBuildingType,
                    backgroundColor: [
                        "#80dda7",
                        "#80dda7",
                        "#80dda7",
                        "#80dda7",
                        "#80dda7",
                        "#80dda7",
                        "#80dda7",
                        "#80dda7",
                        "#80dda7",
                    ],
                };

                var chartOptions = {
                    legend: {
                        display: false,
                        labels: {
                            fontSize: 16,
                        },
                    },
                    plugins: {},
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display:false
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                display:false
                            }   
                        }]
                    }
                };

                new Chart(ctx, {
                    type: "bar",
                    data: {
                        labels: response.label,
                        datasets: [listBuildingType],
                    },
                    options: chartOptions,
                });
            }
        });

    }

</script>
@endpush