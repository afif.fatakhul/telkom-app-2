@extends('layouts.master')
@section('content')
<div class="row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent mb-3 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Data ABD</li>
        </ol>
    </nav>
    <div class="col-12">
        <div class="card mb-4">
            <div class="row">
                <div class="col-6">
                    <div class="card-header pb-0">
                        <h6>Data ABD</h6>
                        <a href="javascript:void(0)" id="btnTambah" class="btn btn-primary">
                            <i class="fas fa-plus"></i>&nbsp;&nbsp;Tambah</a>
                    </div>
                </div>
            </div>
            <div class="card-body px-0 pt-1 pb-2">
                <div class="container">
                    <div class="table-responsive pb-2">
                        <table id="tabel" class="table align-items-center mb-0">
                            <thead class="table table-dark">
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">No.</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site ID</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Site Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Keterangan</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Tanggal</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($abd as $data)
                                <tr>
                                    <td><span class="text-xs font-weight-bold">{{$no++}}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->site_id }}</span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->site_name }}</span></td>
                                    <td><span class="text-xs font-weight-bold"><a href="{{$data->ket}}" target="_blank">{{$data->ket}}</a></span></span></td>
                                    <td><span class="text-xs font-weight-bold">{{ $data->created_at }}</span></td>
                                    <td class="align-middle">
                                        <a href="data-abd/detail/{{$data->id}}" class="btn btn-success mb-0"><i class="fas fa-info me-2" aria-hidden="true"></i>Detail</a>
                                        <a href="javascript:void(0)" class="btn btn-warning mb-0" id="btnEdit" data-toggle="modal" data-id="{{ $data->id }}"><i class="fas fa-pencil-alt me-2" aria-hidden="true"></i>Edit</a>
                                        <meta name=" csrf-token" content="{{ csrf_token() }}">
                                        <a href="javascript:void(0)" class="btn btn-danger mb-0" id="btnHapus" data-toggle="modal" data-id="{{ $data->id }}"><i class="far fa-trash-alt me-2"></i> Hapus</a>
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade bd-example-modal-lg" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambahLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambahLabel"></h5>
            </div>
            <form action="{{route('abdTambah')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="action" id="action" value="">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="s" class=" form-control-label">Pilih Site ID</label>
                                <input class="form-control" list="datalistSite" id="site_id" name="site_id" placeholder="Type to search...">
                                <datalist id="datalistSite">
                                    @foreach($potency as $data)
                                    <option value="{{$data->site_id}}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Site Name</label>
                                <input type="text" id="site_name" name="site_name" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nb" class=" form-control-label">Pilih File <span class="text-xs text-danger font-weight-bold">(*pdf)</span></label>
                                <input type="file" id="file" name="file" placeholder="Masukkan File" class="form-control" accept=".pdf">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="kb" class="form-control-label">Keterangan <span class="text-xs text-danger font-weight-bold">(*Isi link GDrive, jika file lebih dari 10mb)</span></label>
                                <input type="text" id="ket" name="ket" placeholder="Masukkan Keterangan" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal tambah & edit -->
<!-- modal hapus -->
<div class="modal fade bd-example-modal-lg" id="hapus" tabindex="-1" role="dialog" aria-labelledby="hapusLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hapusLabel"></h5>
            </div>
            <form action="{{route('abdHapus')}}" method="POST">
                @csrf
                <input type="hidden" name="id1" id="id1">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ya</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#site_id').on('change', function() {
            $.post('{{ route("getSiteName") }}', {
                _token: '{{csrf_token()}}',
                site_id: this.value
            }, function(data) {
                $('#site_name').val(data.site_name);
            });
        });

        $('#file').bind('change', function() {
            if (this.files[0].size >= '10000000') {
                swal("Error", "File Lebih Dari 10mb!", "error");
                $('#file').val('');
            }
        });

        $('#tabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv'
            ]
        });

        // modal tambah
        $(document).on('click', '#btnTambah', function(e) {
            $('#tambahLabel').html("Tambah Data ABD");
            $('#tambah').modal('show');
            $('input[name=action]').val('tambah');
            $('#id').val("");
            $('#site_id').val("");
            $('#site_name').val("");
            $('#file').val("");
            $('#ket').val("");
        });

        // modal edit
        $('body').on('click', '#btnEdit', function() {
            var data_id = $(this).data('id');
            $.get('data-abd/' + data_id + '/edit', function(data) {
                $('#tambahLabel').html("Edit Data ABD");
                $('#btn-save').prop('disabled', false);
                $('#tambah').modal('show');
                $('input[name=action]').val('edit');
                $('#id').val(data.id);
                $('#site_id').val(data.site_id);
                $('#site_name').val(data.site_name);
                $('#ket').val(data.ket);
                $('#file').val(data.file);
            })
        });

        // modal hapus
        $('body').on('click', '#btnHapus', function() {
            var data_id = $(this).data('id');
            $.get('data-abd/' + data_id + '/edit', function(data) {
                $('#hapusLabel').html("Hapus Data ABD");
                $('#btn-save').prop('disabled', false);
                $('#hapus').modal('show');
                $('#id1').val(data.id);
            })
        });
    });
</script>
@endpush