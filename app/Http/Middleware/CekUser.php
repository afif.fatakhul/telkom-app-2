<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use Alert;

class CekUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role)
    {
        if(Auth::user()->role == "admin" || Auth::user()->role == "guest"){
            return $next($request);
        }else if(Auth::user()->role == $role){
            return $next($request);
        }
        Alert::error('Gagal', 'Anda tidak punya akses untuk mengakses halaman ini!');
        return redirect('/dashboard');
    }
}
