<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
use Alert;
use Illuminate\Support\Facades\Auth;
use Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $rules = [
            'email'                   => 'required',
            'password'                => 'required',
        ];

        $messages = [
            'email.required'             => 'Email harus diisi',
            'password.required'          => 'Password harus diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        if (auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))) {
            Alert::success('Sukses', 'Berhasil Login');
            return redirect('/dashboard');
        } else {
            Alert::error('Gagal', 'Akun Belum Terdaftar');
            return redirect()->route('login');
        }
    }
}
