<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Validator;
use Alert;
use File;

class ABDController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $abd = DB::table('data_abd')->get();
        $potency = DB::table('data_potency')->get();

        return view('admin.data-abd', [
            'abd'  => $abd,
            'potency'  => $potency,
        ]);
    }
    public function getSiteName(Request $request)
    {
        $dp = DB::table('data_potency')->where('site_id', $request->site_id)->first();

        return Response::json($dp);
    }

    public function detail($id)
    {
        $abd = DB::table('data_abd')->where('id', $id)->first();

        return view('admin.detail-data-abd', [
            "abd" => $abd
        ]);
    }

    public function store(Request $request)
    {

        if ($request->action == 'tambah') {

            if ($request->file('file') != "") {
                $name = date('dmYHis') . "-$request->site_name";
                $file = $request->file('file');
                $tujuan_upload = 'file-abd'; //nama folder
                $file->move($tujuan_upload, $name . '.' . $file->getClientOriginalExtension());

                DB::table('data_abd')->insert([
                    'site_id'   => $request->site_id,
                    'site_name'   => $request->site_name,
                    'file'           => $name . '.' . $file->getClientOriginalExtension(),
                    'ket'     => $request->ket,
                ]);
            } else {
                DB::table('data_abd')->insert([
                    'site_id'   => $request->site_id,
                    'site_name'   => $request->site_name,
                    'ket'     => $request->ket,
                ]);
            }

            Alert::success('Sukses', 'Data Berhasil Ditambah');
            return redirect("/data-abd");
        } else if ($request->action == 'edit') {

            if ($request->file('file') != "") {
                $name = date('dmYHis') . "-$request->site_nama";
                $file = $request->file('file');
                $tujuan_upload = 'file-abd'; //nama folder
                $file->move($tujuan_upload, $name . '.' . $file->getClientOriginalExtension());

                DB::table('data_abd')->where('id', $request->id)->update([
                    'site_id'   => $request->site_id,
                    'site_name'   => $request->site_name,
                    'file'           => $name . '.' . $file->getClientOriginalExtension(),
                    'ket'     => $request->ket,
                ]);
            } else {
                DB::table('data_abd')->where('id', $request->id)->update([
                    'site_id'   => $request->site_id,
                    'site_name'   => $request->site_name,
                    'ket'     => $request->ket,
                ]);
            }

            Alert::success('Sukses', 'Data Berhasil Diedit');
            return redirect("/data-abd");
        }
    }

    public function edit($id)
    {
        $dp = DB::table('data_abd')->where('id', $id)->first();

        return Response::json($dp);
    }

    public function destroy(Request $request)
    {
        $dt = DB::table('data_abd')->where('id', $request->id1)->first();
        File::delete('file-abd/' . $dt->file);

        DB::table('data_abd')->where('id', $request->id1)->delete();

        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect("/data-abd");
    }
}
