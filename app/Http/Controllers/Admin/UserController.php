<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Validator;
use Alert;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = DB::table('users')
            ->get();

        return view('admin.data-users', [
            'user'  => $user,
        ]);
    }
    public function cek(Request $request)
    {
        $user = DB::table('users')->where('name', $request->name)->get();

        return Response::json($user);
    }

    public function store(Request $request)
    {

        if ($request->action == 'tambah') {

            $rules = [
                'name'                   => 'required|unique:users'
            ];

            $messages = [
                'name.unique'              => 'Name sudah ada',
                'name.required'            => 'Name wajib diisi',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->all);
            }

            DB::table('users')->insert([
                'name'           => $request->name,
                'email'   => $request->email,
                'password'     => Hash::make($request->password),
                'role'     => $request->role,
            ]);

            Alert::success('Sukses', 'Data Berhasil Ditambah');
            return redirect("/data-users");
        } else if ($request->action == 'edit') {

            if ($request->password != "") {
                DB::table('users')->where('id', $request->id)->update([
                    'name'           => $request->name,
                    'email'   => $request->email,
                    'password'     => Hash::make($request->password),
                    'role'     => $request->role,
                ]);
            } else {
                DB::table('users')->where('id', $request->id)->update([
                    'name'           => $request->name,
                    'email'   => $request->email,
                    'role'     => $request->role,
                ]);
            }

            Alert::success('Sukses', 'Data Berhasil Diedit');
            return redirect("/data-users");
        }
    }

    public function edit($id)
    {
        $dp = DB::table('users')->where('id', $id)->first();

        return Response::json($dp);
    }

    public function destroy(Request $request)
    {
        DB::table('users')->where('id', $request->id1)->delete();

        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect("/data-users");
    }
}
