<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Validator;
use Alert;
use App\Exports\DataPotencyImport;
use Maatwebsite\Excel\Facades\Excel;
use File;

class DataPotencyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dp = DB::table('data_potency')->get();

        $desa = DB::table('data_desa')->groupBy('desa')->get();
        $kec = DB::table('data_desa')->groupBy('kec')->get();
        $kab = DB::table('data_desa')->groupBy('kab')->get();
        $prov = DB::table('data_desa')->groupBy('prov')->get();

        return view('admin.data-potency', [
            'dp'  => $dp,
            'desa'  => $desa,
            'kec'  => $kec,
            'kab'  => $kab,
            'prov'  => $prov,
        ]);
    }

    public function graphPKS(Request $request)
    {
        if ($request->ajax()) {
            $items = DB::table('data_potency')
                ->select(DB::raw('count(*) as total, pks'))
                ->groupBy('pks')
                ->get();

            return response()->json([
                'data' => $items
            ]);
        }
    }

    public function graphCategory(Request $request)
    {
        if ($request->ajax()) {
            $items = DB::table('data_potency')
                ->select(DB::raw('count(*) as total, category'))
                ->groupBy('category')
                ->get();

            return response()->json([
                'data' => $items
            ]);
        }
    }

    public function graphOwner(Request $request)
    {
        if ($request->ajax()) {
            $items = DB::table('data_potency')
                ->select(DB::raw('count(*) as total, owner'))
                ->groupBy('owner')
                ->get();

            return response()->json([
                'data' => $items
            ]);
        }
    }

    public function cek(Request $request)
    {
        $dp = DB::table('data_potency')->where('site_id', $request->site_id)->get();

        return Response::json($dp);
    }

    public function store(Request $request)
    {

        if ($request->action == 'tambah') {

            $rules = [
                'site_id'                   => 'required|unique:data_potency'
            ];

            $messages = [
                'site_id.unique'              => 'Site ID sudah ada',
                'site_id.required'            => 'Site ID wajib diisi',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->all);
            }

            DB::table('data_potency')->insert([
                'site_id'   => $request->site_id,
                'site_name'           => $request->site_name,
                'colocation'     => $request->colocation,
                'system'     =>  implode(' ', $request->system),
                'building_name'     => $request->building_name,
                'category'     => $request->category,
                'util_category'     => $request->util_category,
                'pks'     => $request->pks,
                'contract'     => $request->contract,
                'start_leased'     => $request->start_leased,
                'end_leased'     => $request->end_leased,
                'profitability'     => $request->profitability,
                'ebit_margin'     => $request->ebit_margin,
                'macro'     => $request->macro,
                'lampsite'     => $request->lampsite,
                'owner'     => $request->owner,
                'desa'     => $request->desa,
                'kec'     => $request->kec,
                'kab'     => $request->kab,
                'prov'     => $request->prov,
                'abd'     => $request->abd,
            ]);

            Alert::success('Sukses', 'Data Berhasil Ditambah');
            return redirect("/data-potency");
        } else if ($request->action == 'edit') {

            DB::table('data_potency')->where('id', $request->id)->update([
                'site_id'   => $request->site_id,
                'site_name'           => $request->site_name,
                'colocation'     => $request->colocation,
                'system'     =>  implode(' ', $request->system),
                'building_name'     => $request->building_name,
                'category'     => $request->category,
                'util_category'     => $request->util_category,
                'pks'     => $request->pks,
                'contract'     => $request->contract,
                'start_leased'     => $request->start_leased,
                'end_leased'     => $request->end_leased,
                'profitability'     => $request->profitability,
                'ebit_margin'     => $request->ebit_margin,
                'macro'     => $request->macro,
                'lampsite'     => $request->lampsite,
                'owner'     => $request->owner,
                'desa'     => $request->desa,
                'kec'     => $request->kec,
                'kab'     => $request->kab,
                'prov'     => $request->prov,
                'abd'     => $request->abd,
            ]);

            Alert::success('Sukses', 'Data Berhasil Diedit');
            return redirect("/data-potency");
        }
    }

    public function edit($id)
    {
        $dp = DB::table('data_potency')->where('id', $id)->first();

        return Response::json($dp);
    }

    public function destroy(Request $request)
    {
        DB::table('data_potency')->where('id', $request->id1)->delete();

        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect("/data-potency");
    }

    public function import(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('import_datas', $nama_file);

        // import data
        Excel::import(new DataPotencyImport, public_path('/import_datas/' . $nama_file));

        // notifikasi dengan session
        Alert::success('Sukses', 'Data Potency Berhasil Diimport');

        // delete file import
        File::delete(public_path('/import_datas/' . $nama_file));

        // alihkan halaman kembali
        return redirect('/data-potency');
    }
}
