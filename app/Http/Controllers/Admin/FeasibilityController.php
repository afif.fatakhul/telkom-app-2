<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\FeasibilityExport;
use App\Exports\FeasibilityImport;

class FeasibilityController extends Controller
{

    public $feasibilities = array();

    public function calculator()
    {
        return view("admin.feasibility_calculator.kalkulasi");
    }

    public function exportExcel(Request $request)
    {
        $capexEQP = $request->capexEQP; 
        $tsaEQP = $request->tsaEQP; 
        $capexCME = $request->capexCME; 
        $capexDepreciation = $request->capexDepreciation;
        $totalCapex = $request->totalCapex; 
        $opexNSR = $request->opexNSR; 
        $opexPower = $request->opexPower;
        $opexOM = $request->opexOM; 
        $opexTransmisi = $request->opexTransmisi; 
        $opexFrekuensi = $request->opexFrekuensi;
        $totalOpex = $request->totalOpex; 
        $avgRevenue = $request->avgRevenue; 
        $profitability = $request->profitability;
        $ebitda = $request->ebitda; 
        $profitabilityWI = $request->profitabilityWI; 
        $ebitMargin = $request->ebitMargin;
        $avgRevMonth = $request->avgRevMonth;
        $ebit = $request->ebit;
        ob_end_clean();
        ob_start();
        $response = Excel::download(new FeasibilityExport($capexEQP,$tsaEQP,$capexCME,$capexDepreciation,$totalCapex,$opexNSR,$opexPower,$opexOM,$opexTransmisi,$opexFrekuensi,$totalOpex,$avgRevenue,$profitability,$ebitda,$profitabilityWI,$ebitMargin,$avgRevMonth,$ebit),'feasibilitycalculator.csv');
        return $response;
    }

    public function importView()
    {
        return view("admin.feasibility_calculator.import-kalkulasi");
    }

    public function prosesImport(Request $request)
    {
        $feasibilities = Excel::toArray(new FeasibilityImport, $request->file('fileExcel'));
        $no = 0;
        foreach($feasibilities[0] as $row){
            $no++;
            $capexDep = $row["capex_eqp"] + $row["tsa_eqp"] + $row["capex_cme"];
            $totalOpex = $row["opex_nsrmonth"] + $row["opex_powermonth"] + $row["opex_transmisimonth"] + $row["opex_frekuensimonth"];
            array_push($this->feasibilities, [
                "no" => $no,
                "capexEQP" => $row["capex_eqp"],
                "tsaEQP" => $row["tsa_eqp"],
                "capexCME" => $row["capex_cme"],
                "capexDep" => $capexDep,
                "totalCapex" => $row["capex_eqp"] + $row["tsa_eqp"] + $row["capex_cme"] + $capexDep,
                "opexNSR" => $row["opex_nsrmonth"],
                "opexPower" => $row["opex_powermonth"],
                "opexOM" => $row["opex_o_mmonth"],
                "opexTransmisi" => $row["opex_transmisimonth"],
                "opexFrekuensi" => $row["opex_frekuensimonth"],
                "totalOpex" => $totalOpex,
                "averageREV" => $row["average_rev_month"],
                "profitability" => $row["average_rev_month"] -  $totalOpex,
                "ebitdaMargin" =>($row["average_rev_month"] -  $totalOpex) / $row["average_rev_month"] * 100,
                "profitabilityInvestement" => ($row["average_rev_month"] - $totalOpex -  $capexDep),
                "ebitMargin" =>  ($row["average_rev_month"] - $totalOpex -  $capexDep) / $row["average_rev_month"] * 100,
                "averageREVMin" => ($totalOpex - $capexDep - $row["tsa_eqp"]) / (1-0.38),
                "ebit" => "38%"
            ]);
        }
        return response()->json(["data" => $this->feasibilities]);
    }

    public function exportCsv()
    {
        $fileName = "Feasibility_Calculator.csv";
        $feasibilities = $this->feasibilities;
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        $columns = array(
            "NO",
            "CAPEX EQP",
            "TSA EQP",
            "CAPEX CME",
            "CAPEX Depreciation (Month/5 Years)",
            "TOTAL CAPEX",
            "OPEX NSR/Month",
            "OPEX Power/Month",
            "OPEX O & M/Month",
            "OPEX Transmisi/Month",
            "OPEX Frekuensi/Month",
            "TOTAL OPEX",
            "AVERAGE REV / Month",
            "PROFITABILITY",
            "EBITDA Margin",
            "AVERAGE REV / Month Min",
            "EBIT",
        );

        $callback = function() use($feasibilities, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($feasibilities as $data) {
                $no = $no + 1;
                $row["NO"] = $no;
                $row["CAPEX EQP"] = $data->capexEQP;
                $row["TSA EQP"] = $data->tsaEQP;
                $row["CAPEX CME"] = $data->capexCME;
                $row["CAPEX Depreciation (Month/5 Years)"] = $data->capexDep;
                $row["TOTAL CAPEX"] = $data->totalCapex;
                $row["OPEX NSR/Month"] = $data->opexNSR;
                $row["OPEX Power/Month"] = $data->opexPower;
                $row["OPEX O & M/Month"] = $data->opexOM;
                $row["OPEX Transmisi/Month"] = $data->opexTransmisi;
                $row["OPEX Frekuensi/Month"] = $data->opexFrekuensi;
                $row["TOTAL OPEX"] = $data->totalOpex;
                $row["AVERAGE REV / Month"] = $data->averageREV;
                $row["PROFITABILITY"] = $data->profitability;
                $row["EBITDA Margin"] = $data->ebitdaMargin;
                $row["PROFITABILTY W INVESTEMENT"] = $data->profitabilityInvestement;
                $row["EBIT Margin"] = $data->ebitMargin;
                $row["AVERAGE REV / Month Min"] = $data->averageREVMin;
                $row["EBIT"] = $data->ebit;

                fputcsv($file, array($row['NO'], $row['CAPEX EQP'], $row['TSA EQP'], $row['CAPEX CME'], $row['CAPEX Depreciation (Month/5 Years)'],
                    $row['TOTAL CAPEX'],  $row['OPEX NSR/Month'],  $row['OPEX Power/Month'],  $row['OPEX O & M/Month'], $row['OPEX Transmisi/Month'],
                    $row['OPEX Frekuensi/Month'], $row['TOTAL OPEX'], $row['"AVERAGE REV / Month'], $row['PROFITABILITY'], $row['EBITDA Margin'],
                    $row["PROFITABILTY W INVESTEMENT"], $row["EBIT Margin"], $row['AVERAGE REV / Month Min'], $row["EBIT"]
                ));
            }

            fclose($file);
        };
        
        return response()->stream($callback, 200, $headers);
    }
}
