<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SniperActivity;
use App\Models\Potency;
use App\Exports\SniperImport;
use Maatwebsite\Excel\Facades\Excel;
use File;

class SniperActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function validation($request)
    {
        $validate_data = $this->validate(
            $request,
            [
                'site_id' => 'required',
                'site_name' => 'required',
                'multiname' => 'required',
                'region' => 'required',
                'sow' => 'required',
                'start_date' => 'required',
                'done_date' => 'required',
                'link_report' => 'required',
            ],
            [
                'site_id.required' => 'site_id harus diisi',
                'site_name.required' => 'site_name harus diisi',
                'multiname.required' => 'multiname harus diisi',
                'region.required' => 'region harus diisi',
                'sow.required' => 'sow harus diisi',
                'link_report.required' => 'link_report harus ada',
                'start_date.required' => 'done_date harus diisi',
                'done_date.required' => 'done_date harus diisi',
            ]
        );
        return $validate_data;
    }

    public function index()
    {
        return view('admin.sniper-activity');
    }

    public function fetchData()
    {
        $sniper = SniperActivity::where('kode', '<>', null)->get();
        return response()->json(["data" =>$sniper]);
    }

    public function fetchSites(Request $request)
    {
        $data = [];
        if($request->has('q')){
            $search = $request->q;
            $data = Potency::select("site_id", "site_name")
                ->where('site_id','LIKE',"%$search%")
                ->get();
        }
        return response()->json($data);
    }

    public function putSiteName(Request $request)
    {
        $data = Potency::select("site_id", "site_name")
            ->where('site_id', $request->siteId)
            ->first();
        return response()->json($data->site_name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validation($request);
        $sniper = SniperActivity::create($request->except(['kode'])+["kode" => generateKode()]);
        return response()->json(["code" => 200, "success" => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);
        $sniper = SniperActivity::find($id);
        $sniper->site_id = $request->site_id;
        $sniper->site_name = $request->site_name;
        $sniper->multiname = $request->multiname;
        $sniper->region = $request->region;
        $sniper->sow = $request->sow;
        $sniper->start_date = $request->start_date;
        $sniper->done_date = $request->done_date;
        $sniper->link_report = $request->link_report;
        $sniper->save();
        return response()->json(["code" => 200, "success" => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sniper = SniperActivity::find($id);
        $sniper->delete();
        return response()->json(["code" => 200, "success" => true]);
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        $file = $request->file('file');
        $nama_file = rand() . $file->getClientOriginalName();
        $file->move('import_datas', $nama_file);
        Excel::import(new SniperImport, public_path('/import_datas/' . $nama_file));
        File::delete(public_path('/import_datas/' . $nama_file));
        return response()->json(["code" => 200, "message" => "success"]);
    }
}
