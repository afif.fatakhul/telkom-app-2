<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Validator;
use Alert;
use App\Exports\HardwareImport;
use App\Exports\HardwareReqImport;
use Maatwebsite\Excel\Facades\Excel;
use File;

class HardwareController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $request = DB::table('data_request_hardware')
            ->get();
        $hardware = DB::table('data_hardware')
            ->get();

        return view('admin.data-hardware', [
            'request'  => $request,
            'hardware'  => $hardware,
        ]);
    }

    public function cek(Request $request)
    {
        $hardware = DB::table('data_hardware')->where('modul_name', $request->modul_name)->get();

        return Response::json($hardware);
    }

    public function store(Request $request)
    {

        if ($request->action == 'tambah') {

            $rules = [
                'modul_name'                   => 'required|unique:data_hardware'
            ];

            $messages = [
                'modul_name.unique'              => 'Modul Name sudah ada',
                'modul_name.required'            => 'Modul Name wajib diisi',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->all);
            }

            DB::table('data_hardware')->insert([
                'modul_name'           => $request->modul_name,
                'type'   => $request->type,
                'vendor'     => $request->vendor,
                'qty'     => $request->qty,
                'unit'     => $request->unit,
                'status_eqp'     => $request->status_eqp,
                'site_id_donor'     => $request->site_id_donor,
                'site_name_donor'     => $request->site_name_donor,
                'site_id_acceptor'     => $request->site_id_acceptor,
                'requestor'     => $request->requestor,
                'objective'     => $request->objective,
                'status'     => $request->status,
            ]);

            Alert::success('Sukses', 'Data Berhasil Ditambah');
            return redirect("/hardware");
        } else if ($request->action == 'edit') {

            DB::table('data_hardware')->where('id', $request->id)->update([
                'modul_name'           => $request->modul_name,
                'type'   => $request->type,
                'vendor'     => $request->vendor,
                'qty'     => $request->qty,
                'unit'     => $request->unit,
                'status_eqp'     => $request->status_eqp,
                'site_id_donor'     => $request->site_id_donor,
                'site_name_donor'     => $request->site_name_donor,
                'site_id_acceptor'     => $request->site_id_acceptor,
                'requestor'     => $request->requestor,
                'objective'     => $request->objective,
                'status'     => $request->status,
            ]);

            Alert::success('Sukses', 'Data Berhasil Diedit');
            return redirect("/hardware");
        }
    }

    public function edit($id)
    {
        $dp = DB::table('data_hardware')->where('id', $id)->first();

        return Response::json($dp);
    }

    public function destroy(Request $request)
    {
        DB::table('data_hardware')->where('id', $request->id1)->delete();

        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect("/hardware");
    }

    public function import(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('import_datas', $nama_file);

        // import data
        Excel::import(new HardwareImport, public_path('/import_datas/' . $nama_file));

        // notifikasi dengan session
        Alert::success('Sukses', 'Data Hardware Berhasil Diimport');

        // delete file import
        File::delete(public_path('/import_datas/' . $nama_file));

        // alihkan halaman kembali
        return redirect('/hardware');
    }

    public function storeReq(Request $request)
    {

        if ($request->actionReq == 'tambah') {

            $rules = [
                'modul_nameReq'                   => 'required'
            ];

            $messages = [
                'modul_nameReq.required'            => 'Modul Name wajib diisi',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->all);
            }

            DB::table('data_request_hardware')->insert([
                'modul_name'           => $request->modul_nameReq,
                'type'   => $request->typeReq,
                'site_id'     => $request->site_idReq,
                'status'     => "Waiting"
            ]);

            Alert::success('Sukses', 'Data Berhasil Ditambah');
            return redirect("/hardware");
        } else if ($request->actionReq == 'edit') {

            DB::table('data_request_hardware')->where('id', $request->idReq)->update([
                'modul_name'           => $request->modul_nameReq,
                'type'   => $request->typeReq,
                'site_id'     => $request->site_idReq,
            ]);

            Alert::success('Sukses', 'Data Berhasil Diedit');
            return redirect("/hardware");
        }
    }

    public function editReq($id)
    {
        $dp = DB::table('data_request_hardware')->where('id', $id)->first();

        return Response::json($dp);
    }

    public function destroyReq(Request $request)
    {
        DB::table('data_request_hardware')->where('id', $request->id1Req)->delete();

        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect("/hardware");
    }

    public function importReq(Request $request)
    {
        // validasi
        $this->validate($request, [
            'fileReq' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('fileReq');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('import_datas', $nama_file);

        // import data
        Excel::import(new HardwareReqImport, public_path('/import_datas/' . $nama_file));

        // notifikasi dengan session
        Alert::success('Sukses', 'Data Request Hardware Berhasil Diimport');

        // delete file import
        File::delete(public_path('/import_datas/' . $nama_file));

        // alihkan halaman kembali
        return redirect('/hardware');
    }
}
