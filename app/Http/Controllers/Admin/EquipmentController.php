<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Equipment;
use App\Exports\EquipmentImport;
use Maatwebsite\Excel\Facades\Excel;
use File;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function validation($request)
    {
        $validate_data = $this->validate(
            $request,
            [
                'onair_date' => 'required',
                'site_id' => 'required',
                'combat_sniper' => 'required',
                'node_b_name' => 'required',
                'node_b_status' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'address' => 'required',
                'tx_type' => 'required',
                'remark_issue' => 'required',
                'remark_site_existing' => 'required',
            ],
            [
                'onair_date.required' => 'onair_date harus diisi',
                'site_id.required' => 'site_id harus diisi',
                'combat_sniper.required' => 'combat_sniper harus diisi',
                'node_b_name.required' => 'node_b_name harus diisi',
                'node_b_status.required' => 'node_b_status harus diisi',
                'link_report.required' => 'link_report harus ada',
                'latitude.required' => 'latitude harus diisi',
                'longitude.required' => 'longitude harus diisi',
                'address.required' => 'address harus diisi',
                'tx_type.required' => 'tx_type harus diisi',
                'remark_issue.required' => 'remark_issue harus diisi',
                'remark_site_existing.required' => 'remark_site_existing harus diisi',
            ]
        );
        return $validate_data;
    }


    public function index()
    {
        return view('admin.equipment');
    }

    public function fetchData()
    {
        $equipment = Equipment::all();
        return response()->json(["data" =>$equipment]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);
        $equipment = Equipment::create($request->all());
        return response()->json(["code" => 200, "success" => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validation($request);
        $equipment = Equipment::find($id);
        $equipment->onair_date = $request->onair_date;
        $equipment->site_id = $request->site_id;
        $equipment->combat_sniper = $request->combat_sniper;
        $equipment->node_b_name = $request->node_b_name;
        $equipment->node_b_status = $request->node_b_status;
        $equipment->latitude = $request->latitude;
        $equipment->longitude = $request->longitude;
        $equipment->address = $request->address;
        $equipment->tx_type = $request->tx_type;
        $equipment->remark_issue = $request->remark_issue;
        $equipment->remark_site_existing = $request->remark_site_existing;
        $equipment->save();
        return response()->json(["code" => 200, "success" => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equipment = Equipment::find($id);
        $equipment->delete();
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        $file = $request->file('file');
        $nama_file = rand() . $file->getClientOriginalName();
        $file->move('import_datas', $nama_file);
        Excel::import(new EquipmentImport, public_path('/import_datas/' . $nama_file));
        File::delete(public_path('/import_datas/' . $nama_file));
        return response()->json(["code" => 200, "message" => "success"]);
    }
}
