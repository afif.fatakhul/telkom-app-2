<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Validator;
use Alert;
use App\Exports\ProposalImport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use File;

class ProposalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $proposal = DB::table('data_proposal')
            ->select('data_proposal.*', 'users.name')
            ->join('users', 'users.id', '=', 'data_proposal.users_id')
            ->get();

        $desa = DB::table('data_desa')->groupBy('desa')->get();
        $kec = DB::table('data_desa')->groupBy('kec')->get();
        $kab = DB::table('data_desa')->groupBy('kab')->get();
        $prov = DB::table('data_desa')->groupBy('prov')->get();

        return view('admin.proposal', [
            'proposal'  => $proposal,
            'desa'  => $desa,
            'kec'  => $kec,
            'kab'  => $kab,
            'prov'  => $prov,
        ]);
    }

    public function store(Request $request)
    {

        if ($request->action == 'tambah') {
            if ($request->file('nodin') != "") {
                $name = date('dmYHis');
                $file = $request->file('nodin');
                $tujuan_upload = 'file-nodin'; //nama folder
                $file->move($tujuan_upload, $name . '.' . $file->getClientOriginalExtension());

                DB::table('data_proposal')->insert([
                    'users_id'   => Auth::user()->id,
                    'long'           => $request->long,
                    'lat'     => $request->lat,
                    'desa'     => $request->desa,
                    'kec'     => $request->kec,
                    'kab'     => $request->kab,
                    'prov'     => $request->prov,
                    'revenue_commitment'     => $request->revenue_commitment,
                    'market_share'     => $request->market_share,
                    'objective'     => $request->objective,
                    'divisi'     => $request->divisi,
                    'sow'     => $request->sow,
                    'total_customer'     => $request->total_customer,
                    'nodin'     => $name . '.' . $file->getClientOriginalExtension(),
                ]);
            } else {
                DB::table('data_proposal')->insert([
                    'users_id'   => Auth::user()->id,
                    'long'           => $request->long,
                    'lat'     => $request->lat,
                    'desa'     => $request->desa,
                    'kec'     => $request->kec,
                    'kab'     => $request->kab,
                    'prov'     => $request->prov,
                    'revenue_commitment'     => $request->revenue_commitment,
                    'market_share'     => $request->market_share,
                    'objective'     => $request->objective,
                    'divisi'     => $request->divisi,
                    'sow'     => $request->sow,
                    'total_customer'     => $request->total_customer,
                ]);
            }

            Alert::success('Sukses', 'Data Berhasil Ditambah');
            return redirect("/proposal");
        } else if ($request->action == 'edit') {

            if ($request->file('nodin') != "") {
                $name = date('dmYHis');
                $file = $request->file('nodin');
                $tujuan_upload = 'file-nodin'; //nama folder
                $file->move($tujuan_upload, $name . '.' . $file->getClientOriginalExtension());

                DB::table('data_proposal')->where('id', $request->id)->update([
                    'long'           => $request->long,
                    'lat'     => $request->lat,
                    'desa'     => $request->desa,
                    'kec'     => $request->kec,
                    'kab'     => $request->kab,
                    'prov'     => $request->prov,
                    'revenue_commitment'     => $request->revenue_commitment,
                    'market_share'     => $request->market_share,
                    'objective'     => $request->objective,
                    'divisi'     => $request->divisi,
                    'sow'     => $request->sow,
                    'total_customer'     => $request->total_customer,
                    'nodin'     => $name . '.' . $file->getClientOriginalExtension(),
                ]);
            } else {
                DB::table('data_proposal')->where('id', $request->id)->update([
                    'long'           => $request->long,
                    'lat'     => $request->lat,
                    'desa'     => $request->desa,
                    'kec'     => $request->kec,
                    'kab'     => $request->kab,
                    'prov'     => $request->prov,
                    'revenue_commitment'     => $request->revenue_commitment,
                    'market_share'     => $request->market_share,
                    'objective'     => $request->objective,
                    'divisi'     => $request->divisi,
                    'sow'     => $request->sow,
                    'total_customer'     => $request->total_customer,
                ]);
            }

            Alert::success('Sukses', 'Data Berhasil Diedit');
            return redirect("/proposal");
        }
    }

    public function edit($id)
    {
        $dp = DB::table('data_proposal')
            ->select('users.name', 'data_proposal.*')
            ->join('users', 'users.id', '=', 'data_proposal.users_id')
            ->where('data_proposal.id', $id)->first();

        return Response::json($dp);
    }

    public function destroy(Request $request)
    {
        DB::table('data_proposal')->where('id', $request->id1)->delete();

        Alert::success('Sukses', 'Data Berhasil Dihapus');
        return redirect("/proposal");
    }

    public function import(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('import_datas', $nama_file);

        // import data
        Excel::import(new ProposalImport, public_path('/import_datas/' . $nama_file));

        // notifikasi dengan session
        Alert::success('Sukses', 'Data Proposal Berhasil Diimport');

        // delete file import
        File::delete(public_path('/import_datas/' . $nama_file));

        // alihkan halaman kembali
        return redirect('/proposal');
    }
}
