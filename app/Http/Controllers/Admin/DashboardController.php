<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Potency;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalSite = Potency::count();
        $lampsite = Potency::where('lampsite', 'Yes')->count();
        $macro = Potency::where('macro', 'Yes')->count();
        $owner = Potency::where('owner', 'Telkomsel')->count();

        return view('admin.dashboard', [
           'totalSite' => $totalSite,
           'lampsite' => $lampsite,
           'macro' => $macro,
           'owner' => $owner
        ]);
    }

    public function topWorstPayload(Request $request)
    {
        $potency = Potency::select('payload','site_id','site_name')
            ->orderBy('payload', $request->sort)
            ->limit($request->limit)
            ->get();
        return response()->json(["data" => $potency]);
    }

    public function ebitMarginSinggle()
    {
        $data = Potency::distinct()
            ->where('multiop', 'Single')
            ->get(['range_ebit_margin']);
        $results = [];
        foreach($data as $row){
            $results['label'][] = $row->range_ebit_margin;
            $results['data'][] = (int) countDataEbit("Single", $row->range_ebit_margin);
        }
        return response()->json($results);
    }

    public function buildingType()
    {
        $data = Potency::selectRaw('category, COUNT(category) as nBuilding')
            ->groupBy('category')
            ->orderBy('nBuilding', 'desc')
            ->get();
        $results = [];
        foreach($data as $row){
            $results['label'][] = $row->category;
            $results['data'][] = (int) $row->nBuilding;
        }
        return response()->json($results);
    }

}
