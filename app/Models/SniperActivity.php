<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SniperActivity extends Model
{
    use HasFactory;
    protected $table = "data_sniper_activities";
    protected $fillable = [
        "kode",
        "site_id",
        "site_name",
        "multiname",
        "region",
        "sow",
        "start_date",
        "done_date",
        "year_done",
        "link_report"
    ];
}
