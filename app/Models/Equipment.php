<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    use HasFactory;
    protected $table = "data_equipment";
    protected $fillable = ["onair_date", "site_id", "combat_sniper", "node_b_name",
        "node_b_status", "latitude", "longitude", "address", "tx_type", "remark_issue",
        "remark_site_existing"];
}
