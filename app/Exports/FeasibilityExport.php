<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class FeasibilityExport implements FromView, WithColumnWidths
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $capexEQP; 
    protected $tsaEQP; 
    protected $capexCME; 
    protected $capexDepreciation;
    protected $totalCapex; 
    protected $opexNSR; 
    protected $opexPower;
    protected $opexOM; 
    protected $opexTransmisi; 
    protected $totalOpex; 
    protected $avgRevenue; 
    protected $profitability;
    protected $ebitda; 
    protected $profitabilityWI; 
    protected $ebitMargin;
    protected $avgRevMonth;
    protected $ebit;

    public function __construct($capexEQP, $tsaEQP, $capexCME, $capexDepreciation, $totalCapex, $opexNSR, $opexPower,$opexOM, $opexTransmisi, 
         $opexFrekuensi, $totalOpex, $avgRevenue, $profitability,$ebitda, $profitabilityWI, $ebitMargin,$avgRevMonth,$ebit
    )
    {
        $this->capexEQP = $capexEQP; 
        $this->tsaEQP = $tsaEQP; 
        $this->capexCME = $capexCME; 
        $this->capexDepreciation = $capexDepreciation;
        $this->totalCapex = $totalCapex; 
        $this->opexNSR = $opexNSR; 
        $this->opexPower = $opexPower;
        $this->opexOM = $opexOM; 
        $this->opexTransmisi = $opexTransmisi; 
        $this->opexFrekuensi = $opexFrekuensi;
        $this->totalOpex = $totalOpex; 
        $this->avgRevenue = $avgRevenue; 
        $this->profitability = $profitability;
        $this->ebitda = $ebitda; 
        $this->profitabilityWI = $profitabilityWI; 
        $this->ebitMargin = $ebitMargin;
        $this->avgRevMonth = $avgRevMonth;
        $this->ebit = $ebit;
    }

    public function view(): View
    {
        return view('admin.feasibility_calculator.excel.exportfeasibility', [
            'capexEQP' => $this->capexEQP,
            'tsaEQP' => $this->tsaEQP,
            'capexCME' => $this->capexCME,
            'capexDepreciation' => $this->capexDepreciation,
            'totalCapex' => $this->totalCapex, 
            'opexNSR' => $this->opexNSR,
            'opexPower' => $this->opexPower,
            'opexOM' => $this->opexOM,
            'opexTransmisi' => $this->opexTransmisi,
            'opexFrekuensi' => $this->opexFrekuensi,
            'totalOpex' => $this->totalOpex,
            'avgRevenue' => $this->avgRevenue, 
            'profitability' => $this->profitability,
            'ebitda' => $this->ebitda, 
            'profitabilityWI' => $this->profitabilityWI,
            'ebitMargin' => $this->ebitMargin,
            'avgRevMonth' => $this->avgRevMonth,
            'ebit' => $this->ebit
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 30,
            'B' => 50,  
            'C' => 20          
        ];
    }

}

?>