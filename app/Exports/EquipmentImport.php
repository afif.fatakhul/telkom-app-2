<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Models\Equipment;

class EquipmentImport implements ToModel, WithValidation
{
    use Importable;

    public function rules(): array
    {
        return [];
    }

    public function customValidationMessages()
    {
        return [];
    }

    public function model(array $row)
    {
        Equipment::create([
            'onair_date' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(intval($row[0]))->format('Y-m-d'),
            'site_id' => $row[1],
            'combat_sniper' => $row[2],
            'node_b_name' => $row[3],
            'node_b_status' => $row[4],
            'latitude' => $row[5],
            'longitude' => $row[6],
            'address' => $row[7],
            'tx_type' => $row[8],
            'remark_issue' => $row[9],
            'remark_site_existing' => $row[10],
        ]);
    }
}
