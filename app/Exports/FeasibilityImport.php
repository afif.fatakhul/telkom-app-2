<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FeasibilityImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        $results = array();
        foreach ($rows as $row) 
        {
            $capexDep = ($row["CAPEX EQP"] + $row["TSA EQP"] + $row["CAPEX CME"]) / 5 / 12;
            $totalOpex = $row["OPEX NSR/Month"] + $row["OPEX Power/Month"] +  $row["OPEX Transmisi/Month"] + $row["OPEX Frekuensi/Month"];
            array_push($results, [
                "capexEQP" => $row["CAPEX EQP"],
                "tsaEQP" => $row["TSA EQP"],
                "capexCME" => $row["CAPEX CME"],
                "capexDep" => $capexDep,
                "totalCapex" => $row["CAPEX EQP"] + $row["TSA EQP"] + $row["CAPEX CME"] + $capexDep,
                "opexNSR" => $row["OPEX NSR/Month"],
                "opexPower" => $row["OPEX Power/Month"],
                "opexOM" => $row["OPEX O & M/Month"],
                "opexTransmisi" => $row["OPEX Transmisi/Month"],
                "opexFrekuensi" => $row["OPEX Frekuensi/Month"],
                "totalOpex" => $totalOpex,
                "averageREV" => $row["AVERAGE REV / Month"],
                "profitability" => $row["AVERAGE REV / Month"] -  $totalOpex,
                "ebitdaMargin" =>($row["AVERAGE REV / Month"] -  $totalOpex) / $row["AVERAGE REV / Month"] * 100,
                "profitabilityInvestement" => ($row["AVERAGE REV / Month"] - $totalOpex -  $capexDep),
                "ebit" =>  ($row["AVERAGE REV / Month"] - $totalOpex -  $capexDep) / $row["AVERAGE REV / Month"] * 100,
                "averageREVMin" => ($totalOpex - $capexDep - $row["TSA EQP"]) / (1-0.38),
                "ebit" => "38%"
            ]);
        }
        return $results;
    }

}