<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Models\SniperActivity;

class SniperImport implements ToModel, WithValidation
{
    use Importable;

    public function rules(): array
    {
        return [];
    }

    public function customValidationMessages()
    {
        return [];
    }

    public function model(array $row)
    {
        SniperActivity::create([
            'kode' => generateKode(),
            'site_id' => $row[0],
            'site_name' => $row[1],
            'multiname' => $row[2],
            'region' => $row[3],
            'sow' => $row[4],
            'start_date' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(intval($row[5]))->format('Y-m-d'),
            'done_date' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(intval($row[6]))->format('Y-m-d'),
            'link_report' => $row[7],
        ]);
    }
}
