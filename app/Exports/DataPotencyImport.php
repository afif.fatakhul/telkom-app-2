<?php

namespace App\Exports;

use App\Models\Potency;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\DB;

class DataPotencyImport implements ToModel, WithValidation
{
    use Importable;

    public function rules(): array
    {
        return [
            '0' => 'required|unique:data_potency,site_id',
            '1' => 'required|unique:data_potency,site_name',
        ];
    }

    public function customValidationMessages()
    {
        return [
            '0.required' => 'Site ID tidak boleh kosong',
            '0.unique' => 'Site ID sudah ada',
            '1.required' => 'Site Name tidak boleh kosong',
            '1.unique' => 'Site Name sudah ada',
        ];
    }

    public function model(array $row)
    {
        $start_leased = intval($row[9]);
        $end_leased = intval($row[10]);

        DB::table('data_potency')->insert([
            'site_id' => $row[0],
            'site_name' => $row[1],
            'colocation'     => $row[2],
            'system'     =>  $row[3],
            'building_name'     => $row[4],
            'category'     => $row[5],
            'util_category'     => $row[6],
            'pks'     => $row[7],
            'contract'     => $row[8],
            'start_leased'     => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($start_leased)->format('Y-m-d'),
            'end_leased'     => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($end_leased)->format('Y-m-d'),
            'profitability'     => $row[11],
            'ebit_margin'     => $row[12],
            'macro'     => $row[13],
            'lampsite'     => $row[14],
            'owner'     => $row[15],
            'desa'     => $row[16],
            'kec'     => $row[17],
            'kab'     => $row[18],
            'prov'     => $row[19],
            'abd'     => $row[20],
        ]);
    }
}
