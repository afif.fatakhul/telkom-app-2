<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\DB;

class HardwareReqImport implements ToModel, WithValidation
{
    use Importable;

    public function rules(): array
    {
        return [
            '0' => 'required',
        ];
    }

    public function customValidationMessages()
    {
        return [
            '0.required' => 'Modul Name tidak boleh kosong'
        ];
    }

    public function model(array $row)
    {
        DB::table('data_request_hardware')->insert([
            'modul_name'           => $row[0],
            'type'   => $row[1],
            'site_id'     => $row[2],
            'status'     => "Waiting",
        ]);
    }
}
