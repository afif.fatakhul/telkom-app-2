<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProposalImport implements ToModel, WithValidation
{
    use Importable;

    public function rules(): array
    {
        return [];
    }

    public function customValidationMessages()
    {
        return [];
    }

    public function model(array $row)
    {
        DB::table('data_proposal')->insert([
            'users_id'   => Auth::user()->id,
            'divisi'     => $row[0],
            'sow'     => $row[1],
            'long'           => $row[2],
            'lat'     => $row[3],
            'desa'     => $row[4],
            'kec'     => $row[5],
            'kab'     => $row[6],
            'prov'     => $row[7],
            'revenue_commitment'     => $row[8],
            'market_share'     => $row[9],
            'objective'     => $row[10],
            'total_customer'     => $row[11],
        ]);
    }
}
