<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Facades\DB;

class HardwareImport implements ToModel, WithValidation
{
    use Importable;

    public function rules(): array
    {
        return [
            '0' => 'required|unique:data_hardware,modul_name',
        ];
    }

    public function customValidationMessages()
    {
        return [
            '0.required' => 'Modul Name tidak boleh kosong',
            '0.unique' => 'Modul Name sudah ada',
        ];
    }

    public function model(array $row)
    {
        DB::table('data_hardware')->insert([
            'modul_name'           => $row[0],
            'type'   => $row[1],
            'vendor'     => $row[2],
            'qty'     => $row[3],
            'unit'     => $row[4],
            'status_eqp'     => $row[5],
            'site_id_donor'     => $row[6],
            'site_name_donor'     => $row[7],
            'site_id_acceptor'     => $row[8],
            'requestor'     => $row[9],
            'objective'     => $row[10],
            'status'     => $row[11],
        ]);
    }
}
