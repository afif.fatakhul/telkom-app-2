<?php

use App\Models\Potency;

function countDataEbit($multiop, $ebit) {
    $result = Potency::where('multiop', $multiop)
        ->where('range_ebit_margin', $ebit)
        ->count();
    return $result;
}

function countBuildingType($tipe) {
    $result = Potency::where('category', $tipe)
        ->count();
    return $result;
}