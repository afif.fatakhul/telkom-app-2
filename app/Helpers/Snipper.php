<?php

use App\Models\SniperActivity;
use Carbon\Carbon;

function cekExistingKode($kode){
    $cek = SniperActivity::where('kode', $kode)->count();
    $status = false;
    if($cek > 0){
        $status = true;
    }
    return $status;
}

function generateKode(){
    $count =  SniperActivity::where('kode','<>',null)->count();
    $kodeBaru = "";
    if($count > 0){
        $ulang = true;
        do{
            $lastRecord = SniperActivity::where('kode','<>',null)->orderBy('id', 'desc')->first();
            $angka = substr($lastRecord->kode,7,7) + 1;
            $kodeBaru = "SNP".Carbon::now()->format('Y').$angka;
            if(cekExistingKode($kodeBaru)){
                $ulang = true;
            }else{
                $ulang = false;
            }
        }while($ulang);
    }else{
        $kodeBaru = "SNP".Carbon::now()->format('Y').'1';
    }
    return $kodeBaru;
}